import React, {lazy} from "react";
import {Route, Routes} from 'react-router-dom';

import 'react-perfect-scrollbar/dist/css/styles.min.css';
import 'bootstrap-daterangepicker/daterangepicker.css';
import 'datatables.net-dt/css/jquery.dataTables.min.css';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import 'nprogress/nprogress.css';

import './styles/main5739.css';


import {getRoutePageComponent} from "./@helper/construct-route-page";

const HomePage = lazy(() => import('./pages/index'));
const CardsPage = lazy(() => import('./pages/Cards'));
const TransactionPage = lazy(() => import('./pages/Transactions'));
const TransactionDetailsPage = lazy(() => import('./pages/TransactionDetails'));
const SettingsPage = lazy(() => import('./pages/Settings'));
const OnlineSupportPage = lazy(() => import('./pages/OnlineSupport'));
const LoginPage = lazy(() => import('./pages/Login'));
const RegisterPage = lazy(() => import('./pages/SignUp'));
const TransferPage = lazy(() => import('./pages/Transfer'));
const TransfersPage = lazy(() => import('./pages/Transfers'));
const RegisterSuccessPage = lazy(() => import('./pages/RegisterSuccess'));
const ForgotPasswordPage = lazy(() => import('./pages/ForgotPassword'));
const ResetPasswordPage = lazy(() => import('./pages/ResetPassword'));

function App() {

    return (

        <Routes>
            <Route path={'/'} element={getRoutePageComponent(<HomePage/>)}/>
            <Route path={'/cards'} element={getRoutePageComponent(<CardsPage/>)}/>
            <Route path={'/transactions'} element={getRoutePageComponent(<TransactionPage/>)}/>
            <Route path={'transactions/:transactionId'} element={getRoutePageComponent(<TransactionDetailsPage/>)}/>
            <Route path={'/settings'} element={getRoutePageComponent(<SettingsPage/>)}/>
            <Route path={'/transfers/new'} element={getRoutePageComponent(<TransferPage/>)}/>
            <Route path={'/transfers'} element={getRoutePageComponent(<TransfersPage/>)}/>
            <Route path={'/supports'} element={getRoutePageComponent(<OnlineSupportPage/>)}/>
            <Route path={'/login'} element={getRoutePageComponent(<LoginPage/>)}/>
            <Route path={'/register'} element={getRoutePageComponent(<RegisterPage/>)}/>
            <Route path={'/register-success'} element={getRoutePageComponent(<RegisterSuccessPage/>)}/>
            <Route path={'/forgot-password'} element={getRoutePageComponent(<ForgotPasswordPage/>)}/>
            <Route path={'/reset-password/:token'} element={getRoutePageComponent(<ResetPasswordPage/>)}/>
        </Routes>
    );
}

export default App;
