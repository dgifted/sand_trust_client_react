import React from 'react';
import {Link} from 'react-router-dom';
import {Helmet} from "react-helmet";

import {useAuthContext} from "../../contexts/auth-context";
import {injectStyle, validators} from "../../@helper/validations";
import {toast} from "react-toastify";
import {useToken} from "../../@hooks/use-token";
import {updateApiConfig} from "../../api";
import {useForm} from "@mantine/form";
import {ErrorMessage} from "../../components/FormHelper";
import {useLogo} from "../../@hooks/use-logo";

function LoginPage() {
    const {signIn} = useAuthContext();
    const {setToken} = useToken();
    const Logo = useLogo('blue');

    const loginForm = useForm({
        initialValues: {
            email: '',
            password: ''
        },
        validate: {
            email: val => ((!val || !validators.email(val)) ? 'Enter valid email' : null),
            password: val => (!val ? 'Enter password' : null)
        }
    });

    const onLogin = (value) => {
        loginForm.validate();
        value = {
            ...value,
            identifier: value.email
        }

        signIn(value)
            .then((res) => {
                setToken(res.token);
                updateApiConfig({
                    authToken: res.token
                });
                loginForm.reset();
                toast.success('Login successful.');
                setTimeout(() => {
                    window.location.replace('/');
                }, 3000);
            })
            .catch(err => {
                toast.error(err.message, {autoClose: false});
                if (Number(err?.code) === 404)
                    loginForm.setFieldError('email', `${err.message}`)
            });

    }

    return (
        <React.Fragment>
            <Helmet>
                <body className={'all-wrapper menu-side with-pattern'}/>
            </Helmet>

            <div className="all-wrapper menu-side with-pattern mt-4">
                <div className="auth-box-w">
                    <div className="logo-w">
                        <a href={process.env.REACT_APP_API_URL}>
                            <img alt="" src={Logo} width="100"/>
                        </a>
                    </div>
                    <h4 className="auth-header">Login Form</h4>

                    <form noValidate onSubmit={loginForm.onSubmit(onLogin)}>
                        <div className="form-group">
                            <label htmlFor="email">Email</label>
                            <input className="form-control"
                                   id="email"
                                   placeholder="Enter your email"
                                   style={loginForm.errors?.email ? injectStyle() : {}}
                                   type={'email'}
                                   {...loginForm.getInputProps('email')}
                            />
                            {loginForm.errors?.email ? <ErrorMessage message={loginForm.errors.email}/> : null}
                            <div className="pre-icon os-icon os-icon-user-male-circle"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="password">Password</label>
                            <input className="form-control"
                                   id="password"
                                   placeholder="Enter your password"
                                   style={loginForm.errors?.password ? injectStyle() : {}}
                                   type="password"
                                   {...loginForm.getInputProps('password')}
                            />
                            {loginForm.errors?.password ? <ErrorMessage message={loginForm.errors.password}/> : null}
                            <div className="pre-icon os-icon os-icon-fingerprint"/>
                        </div>
                        <div className="buttons-w">
                            <button className="btn btn-primary mr-3" type="submit">Log in</button>
                            <Link to="/register">
                                Register
                            </Link>
                        </div>
                        <div className="mt-3">
                            <Link to="/forgot-password">Forgot Password</Link>
                        </div>
                    </form>
                </div>
            </div>
        </React.Fragment>
    );
}

export default LoginPage;
