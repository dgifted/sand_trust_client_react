import React from 'react';
import {Helmet} from "react-helmet";
import {Link, useParams, useNavigate} from "react-router-dom";
import {useForm} from '@mantine/form';

import {injectStyle} from "../../@helper/validations";
import {ErrorMessage} from "../../components/FormHelper";
import {authService} from "../../services/auth";
import {toast} from "react-toastify";
import {useLogo} from "../../@hooks/use-logo";
import {alertService} from "../../services/alert";

function ResetPassword() {
    const Logo = useLogo();
    const {token} = useParams();
    const navigate = useNavigate();

    const resetForm = useForm({
        initialValues: {
            password: '',
            passwordConfirm: ''
        },
        validate: {
            password: val => (!val || val.length < 8) ? 'Password should be at least 8 characters' : null,
            passwordConfirm: (val, vals) => vals.password !== val
                ? 'Passwords do not match'
                : !val
                    ? 'Confirm password is required'
                    : null
        }
    });

    const onSubmit = async (value) => {
        resetForm.validate();
        console.log('Form value: ', value);

        try {
            await authService.resetPassword(value, token);
            resetForm.reset();

            alertService({
                title: 'Success',
                message: 'Password reset successfully'
            }).then(() => navigate('/login'));
        } catch (e) {
            toast.error(e.message, { autoClose: false});
        }
    };

    return (
        <React.Fragment>
            <Helmet>
                <body className={'all-wrapper menu-side with-pattern'}/>
            </Helmet>

            <div className="all-wrapper menu-side with-pattern mt-4">
                <div className="auth-box-w">
                    <div className="logo-w">
                        <Link to={'/'}>
                            <img alt="" src={Logo} width="100"/>
                        </Link>
                    </div>
                    <h4 className="auth-header">User password reset</h4>

                    <form noValidate onSubmit={resetForm.onSubmit(onSubmit)}>
                        <div className="form-group">
                            <label htmlFor="password">Password</label>
                            <input className="form-control"
                                   id="password"
                                   placeholder="Enter new password"
                                   style={resetForm.errors?.password ? injectStyle() : {}}
                                   type="password"
                                   {...resetForm.getInputProps('password')}
                            />
                            {resetForm.errors?.password ?
                                <ErrorMessage message={resetForm.errors.password}/> : null}
                            <div className="pre-icon os-icon os-icon-fingerprint"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="passwordConfirm">Confirm Password</label>
                            <input className="form-control"
                                   id="passwordConfirm"
                                   placeholder="Confirm new password"
                                   style={resetForm.errors?.passwordConfirm ? injectStyle() : {}}
                                   type="password"
                                   {...resetForm.getInputProps('passwordConfirm')}
                            />
                            {resetForm.errors?.passwordConfirm ?
                                <ErrorMessage message={resetForm.errors.passwordConfirm}/> : null}
                            <div className="pre-icon os-icon os-icon-fingerprint"/>
                        </div>
                        <div className="buttons-w">
                            <button className="btn btn-primary mr-3" type="submit">Update password</button>
                            <Link to="/login">
                                Login
                            </Link>
                        </div>
                    </form>
                </div>
            </div>
        </React.Fragment>
    );
}

export default ResetPassword;
