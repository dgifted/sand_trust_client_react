import React from 'react';
import useSWR, {useSWRConfig} from 'swr';
import {MdPlaylistAdd} from "react-icons/md";
import {useModals} from "@mantine/modals";

import Layout from "../../components/Container/Layout";
import {pageMetaOptions} from "../../data";
import DebitCard from "../../components/UI/DebitCard";
import Select from "react-select";
import {cancelCardRequest, requestCard} from "../../api/cards";
import {toast} from "react-toastify";
import {useCards} from "../../@hooks/use-cards";
import {useAuthUser} from "../../@hooks/use-auth-user";
import EmptyList from "../../components/UI/EmptyList";
import DataTableBase from "../../components/Container/DataTableBase";
import {cardNumberFormat} from "../../services/card-number-format";
import {activateUserCard, deactivateUserCard} from "../../services/cards";

const metaOptions = {
    ...pageMetaOptions,
    title: 'Cards'
};

function CardsPage() {
    const toastRef = React.useRef(null);
    const {data} = useSWR('api/card-types');
    const {data: pendingRequest, error: pendingRequestError} = useSWR('api/card/active-request');
    const {data: cards} = useCards();
    const {data: userData} = useAuthUser();
    const {data: userProfileData} = useSWR('api/user-profile');
    // const [pendingCardRequest, setPendingCardRequest] = React.useState(!!pendingRequest && !pendingRequestError);
    const modals = useModals();
    const {mutate} = useSWRConfig();

    const cardTypeOptions = [];
    const userCards = cards?.data?.data;
    const user = userData?.data;
    const balance = userProfileData?.stats?.accsDetails?.balance;
    const currency = userProfileData?.stats?.accsDetails?.currency;
    const pendingCardRequest = !!pendingRequest && !pendingRequestError;


    if (data) {
        data.forEach(item => {
            cardTypeOptions.push({
                value: item.id,
                label: item.name.toUpperCase()
            })
        })
    }

    const onRequestHandler = (openModalId, selected) => {
        const payload = {cardType: selected};
        toastRef.current = toast.info('Please wait...', {autoClose: false});
        requestCard(payload)
            .then(() => {
                modals.closeModal(openModalId);
                // setPendingCardRequest(true);
                toast.update(toastRef.current, {
                    render: 'Card request successfully.',
                    autoClose: 3000,
                    type: toast.TYPE.SUCCESS
                });
            })
            .catch(error => {
                toast.update(toastRef.current, {
                    render: `${error.message}`,
                    autoClose: 3000,
                    type: toast.TYPE.ERROR
                })
            });
    };

    const onCancelRequestHandler = () => {
        const cardRefId = pendingRequest.data.ref_id;
        if (!cardRefId)
            return;

        cancelCardRequest(cardRefId)
            .then(() => {
                toastRef.current = toast.info('Please wait...', {autoClose: false});
                // setPendingCardRequest(false);
                toast.update(toastRef.current, {
                    render: 'Card request cancelled successfully.',
                    autoClose: 3000,
                    type: toast.TYPE.SUCCESS
                });
            })
            .catch(error => {
                toast.update(toastRef.current, {
                    render: `${error.message}`,
                    autoClose: 3000,
                    type: toast.TYPE.ERROR
                })
            })
    }

    const openModal = () => {
        let selected;
        const id = modals.openModal({
            title: 'Card Request',
            children: (
                <div className="d-flex flex-column">
                    <label className={'text-capitalize'}>Select card type</label>
                    <Select
                        defaultValue={null}
                        onChange={option => selected = option.value}
                        options={cardTypeOptions}
                    />
                    <button className={'btn btn-primary mt-4'} onClick={() => {
                        onRequestHandler(id, selected);
                    }}>
                        Confirm
                    </button>
                </div>
            )
        })
    }

    const userCardsTableCols = [
        {name: 'Type', selector: row => row.type},
        {name: 'Bank Name', selector: row => row.bankName},
        {name: 'Name on card', selector: row => row.holderName},
        {name: 'Number', selector: row => row.cardNumber},
        {name: 'Valid thru', selector: row => row.validTru},
        {name: 'Status', selector: row => row.status},
        {
            cell: (card) => (<button
                className={`btn btn-sm ${card.status.props.children === 'Active' ? 'btn-danger' : 'btn-success'}`}
                onClick={() => {
                    card.status.props.children === 'Active' ? onDeactivateCard(card.id) : onActivateCard(card.id)
                }}>{card.status.props.children === 'Active' ? 'Block' : 'Unblock'}
            </button>),
            ignoreRowClick: true,
            allowOverflow: true,
            button: true,
        }
    ];

    const userCardsTableData = () => {
        if (!userCards?.length)
            return [];

        return userCards?.map((card) => ({
            id: card?.id,
            type: card?.card_type?.name.toUpperCase(),
            bankName: process.env.REACT_APP_SITE_NAME,
            holderName: user?.first_name + ' ' + user?.last_name,
            cardNumber: cardNumberFormat(card?.number),
            validTru: card?.expires_month + '/' + card?.expires_year,
            status: (<div className={`badge ${card?.is_active == 1 ? 'badge-success' : 'badge-danger'}`}>
                {card?.is_active ? 'Active' : 'Deactivated'}
            </div>)
        }));
    };

    const onActivateCard = async (cardId) => {
        const {error} = await activateUserCard(cardId);

        if (error) {
            return toast.error(error.message);
        }
        toast.success('Card unblocked successful.');
        await mutate('api/card/user-cards');
    }

    const onDeactivateCard = async (cardId) => {
        const {error} = await deactivateUserCard(cardId);
        if (error) {
            return toast.error(error.message);
        }
        toast.success('Card blocked successful.');
        await mutate('api/card/user-cards');
    }

    return (
        <Layout {...metaOptions}>
            <div className="element-wrapper">
                <div className="element-actions">
                    <button
                        className={'btn btn-primary btn-rounded'}
                        onClick={pendingCardRequest ? onCancelRequestHandler : openModal}
                        title={`${pendingCardRequest ? 'You have a pending card request' : 'Request new card'}`}
                    >
                        <MdPlaylistAdd size={'0.9rem'}/>{' '}
                        {`${pendingCardRequest ? 'Cancel pending request' : 'Request new card'}`}
                    </button>
                </div>
                <h6 className="element-header">Cards</h6>
                <div className="element-content">
                    <div className="row">
                        {userCards?.length > 0
                            ? userCards.map(card => (
                                <div className={'col-12 col-sm-6 col-lg-4'} key={card.id.toString()}>
                                    <DebitCard
                                        number={card.number}
                                        holderName={user?.first_name + ' ' + user?.last_name}
                                        balance={balance}
                                        validTru={`${card?.expires_month}/${card?.expires_year}`}
                                        currency={currency}
                                        blocked={Number(card?.is_active) !== 1}
                                    />
                                </div>
                            ))
                            : <EmptyList message="Your account do not have any debit card"/>
                        }
                    </div>
                </div>
            </div>
            <div className="element-wrapper">
                <h6 className="element-header">Card List</h6>
                <div className="element-box">
                    <div className="table-responsive">
                        <DataTableBase
                            columns={userCardsTableCols}
                            data={userCardsTableData()}
                        />
                    </div>
                </div>
            </div>
        </Layout>
    );
}

export default CardsPage;
