import React from 'react';
import useSWR from 'swr';
import {Link, useNavigate} from 'react-router-dom';
import {Helmet} from "react-helmet";
import {toast} from "react-toastify";

import {useLogo} from "../../@hooks/use-logo";
import {useForm} from "@mantine/form";
import {injectStyle, validators} from "../../@helper/validations";
import {useAuthContext} from "../../contexts/auth-context";
import {ErrorMessage, RequiredMarker} from "../../components/FormHelper";

function SignUpPage() {
    const Logo = useLogo('blue');
    const {data: accountTypesData} = useSWR('api/account-types');
    const {data: countriesData} = useSWR('api/countries');
    const {data: currenciesData} = useSWR('api/currencies');
    const {signUp} = useAuthContext();
    const [regSuccess, setRegSuccess] = React.useState(false);
    const navigate = useNavigate();

    const registerForm = useForm({
        initialValues: {
            fullName: '',
            email: '',
            accountType: '',
            currency: '',
            dob: '',
            gender: '',
            maritalStatus: '',
            spouseName: '',
            nationality: '',
            occupation: '',
            monthlyIncome: '',
            sourceIncome: '',
            address1: '',
            address2: '',
            city: '',
            state: '',
            zipCode: '',
            country: ''
        },
        validate: {
            fullName: val => (!val ? 'Full name is required.' : null),
            email: val => (!validators.email(val) || !val ? 'Enter valid email.' : null),
            accountType: val => (!val ? 'Choose a suitable account type' : null),
            currency: val => (!val ? 'Currency is required' : null),
            dob: val => (!val ? 'Date of birth is required.' : null),
            gender: val => (!val ? 'Gender is required.' : null),
            address1: val => (!val ? 'Address 1 is required' : null)
        }
    });

    const onRegister = async (value) => {
        setRegSuccess(false);
        registerForm.validate();

        try {
            await signUp(value);
            registerForm.reset();
            toast.success('Registration successful.');
            setRegSuccess(true);
            navigate('/register-success');
        } catch (e) {
            toast.error(e.message);
        }
    }


    return (
        <React.Fragment>
            <Helmet>
                <body className={'all-wrapper menu-side with-pattern'}/>
            </Helmet>

            <div className="all-wrapper menu-side with-pattern mt-4">

                <div className="auth-box-w wider">
                    <div className="logo-w" style={{maxHeight: '100px'}}>
                        <a href={process.env.REACT_APP_API_URL}>
                            <img alt="" src={Logo} width="100"/>
                        </a>
                    </div>
                    {regSuccess ? (
                        <div className="px-4">
                            <div className="alert alert-success">
                                Your account has been successfully created. Check your inbox for login password.
                            </div>
                        </div>

                    ) : null}
                    <h4 className="auth-header">Create new account</h4>
                    <form noValidate onSubmit={registerForm.onSubmit(onRegister)}>
                        <div className="row">
                            <div className="col-sm-6">
                                <div className="form-group">
                                    <label htmlFor="fullName">Full name <RequiredMarker/></label>
                                    <input className="form-control"
                                           id="fullName"
                                           placeholder="Enter full name"
                                           type="text"
                                           style={registerForm.errors?.fullName ? injectStyle() : {}}
                                           {...registerForm.getInputProps('fullName')}
                                    />
                                    {registerForm.errors?.fullName ?
                                        <ErrorMessage message={registerForm.errors.fullName}/> : null}
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <div className="form-group">
                                    <label htmlFor="email">Email <RequiredMarker/></label>
                                    <input className="form-control"
                                           id="email"
                                           placeholder="Enter email"
                                           type="email"
                                           style={registerForm.errors?.email ? injectStyle() : {}}
                                           {...registerForm.getInputProps('email')}
                                    />
                                    {registerForm.errors?.email ?
                                        <ErrorMessage message={registerForm.errors.email}/> : null}
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-6">
                                <div className="form-group">
                                    <label htmlFor="accountType">Account Type <RequiredMarker/></label>
                                    <select
                                        className="form-control"
                                        id="accountType"
                                        style={registerForm.errors?.accountType ? injectStyle() : {}}
                                        {...registerForm.getInputProps('accountType')}
                                    >
                                        <option value="">Select account type</option>
                                        {!!accountTypesData?.length ? (
                                            accountTypesData.map(type => (
                                                <option key={type.id.toString()} value={type.id}>
                                                    {type.title.toUpperCase()}
                                                </option>
                                            ))
                                        ) : null}
                                    </select>
                                    {registerForm.errors?.accountType ?
                                        <ErrorMessage message={registerForm.errors.accountType}/> : null}
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <div className="form-group">
                                    <label htmlFor="currency">Currency <RequiredMarker/></label>
                                    <select
                                        className="form-control"
                                        id="currency"
                                        style={registerForm.errors?.currency ? injectStyle() : {}}
                                        {...registerForm.getInputProps('currency')}
                                    >
                                        <option value="">Select currency</option>
                                        {!!currenciesData?.length ? (
                                            currenciesData.map(currency => (
                                                <option key={currency.id.toString()} value={currency.id}>
                                                    {currency.name} - ({currency.abbreviation.toUpperCase()})
                                                </option>
                                            ))
                                        ) : null}
                                    </select>
                                    {registerForm.errors?.currency ?
                                        <ErrorMessage message={registerForm.errors.currency}/> : null}
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-sm-6">
                                <div className="form-group">
                                    <label htmlFor="dob">Date of Birth <RequiredMarker/></label>
                                    <input className="form-control"
                                           id="dob"
                                           placeholder="Enter date of birth"
                                           type="date"
                                           style={registerForm.errors?.dob ? injectStyle() : {}}
                                           {...registerForm.getInputProps('dob')}
                                    />
                                    {registerForm.errors?.dob ?
                                        <ErrorMessage message={registerForm.errors.dob}/> : null}
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <div className="form-group">
                                    <label htmlFor="gender">Gender <RequiredMarker/></label>
                                    <select
                                        className="form-control"
                                        id="gender"
                                        style={registerForm.errors?.gender ? injectStyle() : {}}
                                        {...registerForm.getInputProps('gender')}
                                    >
                                        <option value="">Select gender</option>
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                    </select>
                                    {registerForm.errors?.gender ?
                                        <ErrorMessage message={registerForm.errors.gender}/> : null}
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-sm-6">
                                <div className="form-group">
                                    <label htmlFor="maritalStatus">Marital status</label>
                                    <select
                                        className="form-control"
                                        id="maritalStatus"
                                        {...registerForm.getInputProps('maritalStatus')}
                                    >
                                        <option value="">Select marital status</option>
                                        <option value="single">Single</option>
                                        <option value="married">Married</option>
                                        <option value="divorced">Divorced</option>
                                        <option value="widowed">Widowed</option>
                                    </select>
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <div className="form-group">
                                    <label htmlFor="spouseName">Spouse name</label>
                                    <input className="form-control"
                                           id="spouseName"
                                           placeholder="Enter spouse name"
                                           type="text"
                                           {...registerForm.getInputProps('spouseName')}
                                    />
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-sm-6">
                                <div className="form-group">
                                    <label htmlFor="nationality">Nationality</label>
                                    <input className="form-control"
                                           id="nationality"
                                           placeholder="Enter nationality"
                                           type="text"
                                           {...registerForm.getInputProps('nationality')}
                                    />
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <div className="form-group">
                                    <label htmlFor="occupation">Occupation</label>
                                    <input className="form-control"
                                           id="occupation"
                                           placeholder="Enter occupation"
                                           type="text"
                                           {...registerForm.getInputProps('occupation')}
                                    />
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-sm-6">
                                <div className="form-group">
                                    <label htmlFor="monthlyIncome">Monthly income</label>
                                    <input className="form-control"
                                           id="monthlyIncome"
                                           placeholder="Enter monthly income"
                                           type="text"
                                           {...registerForm.getInputProps('monthlyIncome')}
                                    />
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <div className="form-group">
                                    <label htmlFor="sourceIncome">Source of income</label>
                                    <input className="form-control"
                                           id="sourceIncome"
                                           placeholder="Enter source of income"
                                           type="text"
                                           {...registerForm.getInputProps('sourceIncome')}
                                    />
                                </div>
                            </div>
                        </div>

                        <div className="form-group">
                            <label htmlFor="address1">Address 1 <RequiredMarker/></label>
                            <input className="form-control"
                                   id="address1"
                                   placeholder="Enter address 1"
                                   type="text"
                                   style={registerForm.errors?.address1 ? injectStyle() : {}}
                                   {...registerForm.getInputProps('address1')}
                            />
                            {registerForm.errors?.address1 ?
                                <ErrorMessage message={registerForm.errors?.address1}/> : null}
                        </div>

                        <div className="form-group">
                            <label htmlFor="address2">Address 2</label>
                            <input className="form-control"
                                   id="address2"
                                   placeholder="Enter address 2"
                                   type="text"
                                   {...registerForm.getInputProps('address2')}
                            />
                        </div>

                        <div className="row">
                            <div className="col-sm-6">
                                <div className="form-group">
                                    <label htmlFor="city">City</label>
                                    <input className="form-control"
                                           id="city"
                                           placeholder="Enter city"
                                           type="text"
                                           {...registerForm.getInputProps('city')}
                                    />
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <div className="form-group">
                                    <label htmlFor="state">State</label>
                                    <input className="form-control"
                                           id="state"
                                           placeholder="Enter nationality"
                                           type="text"
                                           {...registerForm.getInputProps('state')}
                                    />
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-sm-6">
                                <div className="form-group">
                                    <label htmlFor="zipCode">Zip code</label>
                                    <input className="form-control"
                                           id="zipCode"
                                           placeholder="Enter zip code"
                                           type="text"
                                           {...registerForm.getInputProps('zipCode')}
                                    />
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <div className="form-group">
                                    <label htmlFor="country">Country</label>
                                    <select className="form-control"
                                            id="country" {...registerForm.getInputProps('country')}>
                                        <option value="">Select country</option>
                                        {!!countriesData?.length ? (
                                            countriesData.map(country => (
                                                <option key={country.id.toString()}
                                                        value={`${country.id}:${country.name}`}>
                                                    {country.name}
                                                </option>
                                            ))
                                        ) : null}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="buttons-w">
                            <button className="btn btn-primary mr-3" type="submit">Register Now</button>
                            <Link className="btn btn-link" to="/login">Login</Link>
                        </div>
                        <div className="mt-3">
                            <Link to="/forgot-password">Forgot Password</Link>
                        </div>
                    </form>
                </div>
            </div>
        </React.Fragment>
    );
}

export default SignUpPage;
