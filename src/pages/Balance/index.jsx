import React from 'react';
import Layout from "../../components/Container/Layout";
import {pageMetaOptions} from "../../data";
import CardDeckSlider from "../../components/CardDeckSlider";
import DebitCard from "../../components/UI/DebitCard";

const metaOption = {
    ...pageMetaOptions,
    title: 'Balance'
}

function Balance() {

    return (
        <Layout {...metaOption}>
            <div className="element-wrapper">
                <h6 className="element-header">Balance</h6>
                <div className="element-content">
                    <div className="tablo-with-chart">
                        <div className="row">
                            <div className="col-sm-5 col-xxl-4">
                                <div className="tablos">
                                    <div className="row mb-xl-2 mb-xxl-3">
                                        <div className="col-sm-6">
                                                        <span
                                                            className="element-box el-tablo centered trend-in-corner padded bold-label"
                                                        >
                                                            <div className="value">24</div>
                                                            <div className="label">New Tickets</div>
                                                            <div className="trending trending-up-basic">
                                                                <span>12%</span><i
                                                                className="os-icon os-icon-arrow-up2"/></div>
                                                        </span>
                                        </div>
                                        <div className="col-sm-6"><span
                                            className="element-box el-tablo centered trend-in-corner padded bold-label">
                                                        <div className="value">12</div>
                                                        <div className="label">Closed Today</div>
                                                        <div className="trending trending-down-basic">
                                                            <span>12%</span><i
                                                            className="os-icon os-icon-arrow-down"/>
                                                        </div>
                                                    </span>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-sm-6"><span
                                            className="element-box el-tablo centered trend-in-corner padded bold-label"
                                        >
                                                        <div className="value">52</div>
                                                        <div className="label">Agent Replies</div>
                                                        <div className="trending trending-up-basic">
                                                            <span>12%</span><i
                                                            className="os-icon os-icon-arrow-up2"/></div>
                                                    </span></div>
                                        <div className="col-sm-6"><span
                                            className="element-box el-tablo centered trend-in-corner padded bold-label"
                                        >
                                                        <div className="value">7</div>
                                                        <div className="label">New Replies</div>
                                                        <div className="trending trending-down-basic">
                                                            <span>12%</span><i
                                                            className="os-icon os-icon-arrow-down"/>
                                                        </div>
                                                    </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="element-wrapper">
                <h6 className="element-header">Recent Transactions</h6>
                <div className="element-box-tp">
                    <div className="table-responsive">
                        <table className="table table-padded">
                            <thead>
                            <tr>
                                <th>Status</th>
                                <th>Date</th>
                                <th>Description</th>
                                <th className="text-center">Category</th>
                                <th className="text-right">Amount</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td className="nowrap"><span
                                    className="status-pill smaller green"></span><span>Complete</span>
                                </td>
                                <td><span>Today</span><span className="smaller lighter">1:52am</span></td>
                                <td className="cell-with-media">
                                    <img alt="" src="../../assets/images/company1.png"
                                         style={{height: "25px"}}/><span>Banana Shakes LLC</span>
                                </td>
                                <td className="text-center"><a className="badge badge-success"
                                                               href="#">Shopping</a></td>
                                <td className="text-right bolder nowrap"><span className="text-success">+ 1,250
                                                        USD</span></td>
                            </tr>
                            <tr>
                                <td className="nowrap"><span
                                    className="status-pill smaller red"></span><span>Declined</span>
                                </td>
                                <td><span>Jan 19th</span><span className="smaller lighter">3:22pm</span>
                                </td>
                                <td className="cell-with-media"><img alt="" src="../../assets/images/company2.png"
                                                                     style={{height: "25px"}}/><span>Stripe Payment Processing</span>
                                </td>
                                <td className="text-center"><a className="badge badge-danger" href="#">Cafe</a>
                                </td>
                                <td className="text-right bolder nowrap"><span className="text-success">+ 952.23
                                                        USD</span></td>
                            </tr>
                            <tr>
                                <td className="nowrap"><span
                                    className="status-pill smaller yellow"/><span>Pending</span>
                                </td>
                                <td><span>Yesterday</span><span className="smaller lighter">7:45am</span>
                                </td>
                                <td className="cell-with-media"><img alt="" src="../../assets/images/company3.png"
                                                                     style={{height: "25px"}}/><span>MailChimp Services</span>
                                </td>
                                <td className="text-center"><a className="badge badge-warning"
                                                               href="#">Restaurants</a></td>
                                <td className="text-right bolder nowrap"><span className="text-danger">- 320
                                                        USD</span></td>
                            </tr>
                            <tr>
                                <td className="nowrap"><span
                                    className="status-pill smaller yellow"/><span>Pending</span>
                                </td>
                                <td><span>Jan 23rd</span><span className="smaller lighter">2:12pm</span>
                                </td>
                                <td className="cell-with-media"><img alt="" src="../../assets/images/company1.png"
                                                                     style={{height: "25px"}}/><span>Starbucks Cafe</span>
                                </td>
                                <td className="text-center"><a className="badge badge-primary"
                                                               href="#">Shopping</a></td>
                                <td className="text-right bolder nowrap"><span className="text-success">+ 17.99
                                                        USD</span></td>
                            </tr>
                            <tr>
                                <td className="nowrap"><span
                                    className="status-pill smaller green"/><span>Complete</span>
                                </td>
                                <td><span>Jan 12th</span><span className="smaller lighter">9:51am</span>
                                </td>
                                <td className="cell-with-media"><img alt="" src="../../assets/images/company4.png"
                                                                     style={{height: "25px"}}/><span>Ebay Marketplace</span>
                                </td>
                                <td className="text-center"><a className="badge badge-danger"
                                                               href="#">Groceries</a></td>
                                <td className="text-right bolder nowrap"><span className="text-danger">- 244
                                                        USD</span></td>
                            </tr>
                            <tr>
                                <td className="nowrap"><span
                                    className="status-pill smaller yellow"/><span>Pending</span>
                                </td>
                                <td><span>Jan 9th</span><span className="smaller lighter">12:45pm</span>
                                </td>
                                <td className="cell-with-media"><img alt="" src="../../assets/images/company2.png"
                                                                     style={{height: "25px"}}/><span>Envato Templates Inc</span>
                                </td>
                                <td className="text-center"><a className="badge badge-primary"
                                                               href="#">Business</a></td>
                                <td className="text-right bolder nowrap"><span className="text-success">+ 340
                                                        USD</span></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>'
                </div>
            </div>
        </Layout>
    );
}

export default Balance;
