import React from 'react';
import {Helmet} from "react-helmet";
import {Link} from "react-router-dom";
import {useForm} from '@mantine/form';

import {useLogo} from "../../@hooks/use-logo";
import {injectStyle, validators} from "../../@helper/validations";
import {ErrorMessage} from "../../components/FormHelper";
import {authService} from "../../services/auth";
import {toast} from "react-toastify";
import {alertService} from "../../services/alert";

function ForgotPassword() {
    const Logo = useLogo('blue');

    const passwordRecoveryForm = useForm({
        initialValues: {
            email: ''
        },
        validate: {
            email: val => (!val || !validators.email(val)) ? 'Enter valid email' : null,
        }
    });

    const onSubmitEmail = async (value) => {
        passwordRecoveryForm.validate();

        try {
            await authService.forgotPassword(value);
            toast.success('Password reset link has been sent to your inbox');
            await alertService({
                title: 'Success',
                message: 'Password reset link has been sent to your inbox'
            });
            passwordRecoveryForm.reset();
        } catch (e) {
            toast.error(e.message)
        }

    };

    return (
        <React.Fragment>
            <Helmet>
                <body className={'all-wrapper menu-side with-pattern'}/>
            </Helmet>

            <div className="all-wrapper menu-side with-pattern mt-4">
                <div className="auth-box-w">
                    <div className="logo-w">
                        <Link to={'/'}>
                            <img alt="" src={Logo} width="100"/>
                        </Link>
                    </div>
                    <h4 className="auth-header">User password recovery</h4>

                    <form noValidate onSubmit={passwordRecoveryForm.onSubmit(onSubmitEmail)}>
                        <div className="form-group">
                            <label htmlFor="email">Email</label>
                            <input className="form-control"
                                   id="email"
                                   placeholder="Enter your email"
                                   style={passwordRecoveryForm.errors?.email ? injectStyle() : {}}
                                   type={'email'}
                                   {...passwordRecoveryForm.getInputProps('email')}
                            />
                            {passwordRecoveryForm.errors?.email ? <ErrorMessage message={passwordRecoveryForm.errors.email}/> : null}
                            <div className="pre-icon os-icon os-icon-user-male-circle"/>
                        </div>
                        <div className="buttons-w">
                            <button className="btn btn-primary mr-3" type="submit">Continue</button>
                            <Link to="/login">
                                Login
                            </Link>
                        </div>
                    </form>
                </div>
            </div>
        </React.Fragment>
    );
}

export default ForgotPassword;
