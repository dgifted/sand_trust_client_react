import React, {useCallback} from 'react';
import {MdOutlinePrint} from "react-icons/md";

import {pageMetaOptions} from "../../data";
import Layout from "../../components/Container/Layout";
import {useLocation, useParams} from "react-router-dom";
import {getUserSingleTransaction} from "../../services/transactions";
import {useAuthUser} from "../../@hooks/use-auth-user";
import Currency from "../../components/Currency";

const metaOptions = {
    ...pageMetaOptions,
    title: 'Transaction details'
};

function TransactionDetailsPage() {
    const {data: userData} = useAuthUser();
    const {transactionId} = useParams();
    const {search} = useLocation();
    const [fetchError, setFetchError] = React.useState(false);
    const [pageData, setPageData] = React.useState();
    const user = userData?.data;

    let type = 'deposit';
    if (search)
        type = search.split('=')[1];

    const fetchPageData = useCallback(async () => {
        const {error, result} = await getUserSingleTransaction(transactionId, type);
        if (error)
            return setFetchError(error.message);

        setPageData(result.data);

    }, [transactionId, type]);

    React.useEffect(() => {
        fetchPageData();
    }, []);


    return (
        <Layout {...metaOptions}>
            <div className="element-wrapper">
                {fetchError ?
                    (
                        <div className="d-flex justify-content-center align-items-center">
                            {fetchError}
                        </div>
                    ) :
                    (
                        <React.Fragment>
                            <div className="element-actions">
                                <button className={'btn btn-primary btn-rounded'} onClick={() => window.print()}>
                                    <MdOutlinePrint size={'0.9rem'}/>{' '}Print
                                </button>
                            </div>
                            <h6 className="element-header">Transactions Details</h6>
                            <div className="element-content">
                                <div
                                    className={'d-flex flex-column flex-md-row justify-content-between align-items-start'}>

                                    <div className={'d-flex flex-column'}>
                                        <p className={'mb-2'}>Recipient</p>
                                        <div>
                                            <p className={'mb-0'}>
                                                {type !== 'deposit' ? (
                                                    <b>{pageData?.payee_name}</b>
                                                ) : (
                                                    <b>{user?.first_name} {user?.last_name}</b>
                                                )}
                                            </p>
                                            <p>
                                                {type !== 'deposit' ? null : (
                                                    <>{user?.email}</>
                                                )}

                                            </p>
                                        </div>
                                    </div>

                                    <div className="d-flex flex-column align-items-center">
                                        <div
                                            className={`text-uppercase bg-${type === 'deposit' ? 'success' : 'danger'}`}
                                            style={{
                                                width: 'auto',
                                                padding: '10px 20px',
                                                color: '#fff',
                                                borderRadius: '20px'
                                            }}
                                        >
                                            {type}
                                        </div>
                                    </div>
                                </div>
                                <hr className={'my-2 my-sm-3 my-md-4'}/>

                                <div className="row">
                                    <div className={'col-12 col-md-6 col-xl-6'}>
                                        <div className={'d-flex flex-column justify-content-between'}>
                                            <p>Due Date</p>
                                            <p className={'text-primary'}>
                                                {new Date(pageData?.updated_at).toDateString()}
                                            </p>
                                        </div>
                                    </div>
                                    <div className={'col-12 col-md-6 col-xl-6'}>
                                        <div
                                            className={'d-flex flex-column justify-content-between align-item-stretch'}>
                                            <div className="d-flex justify-content-between">
                                                <strong>Sub Total</strong>
                                                <span><Currency/>{pageData?.amount.toLocaleString()}</span>
                                            </div>
                                            <div className="d-flex justify-content-between my-3">
                                                <b>Tax</b>
                                                <span>N/A</span>
                                            </div>
                                            <div className="d-flex justify-content-between">
                                                <b>Total</b>
                                                <span><Currency/>{pageData?.amount.toLocaleString()}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr className={'my-2 my-sm-3 my-md-4'}/>

                                {/*<div className={'mt-5 pt-3'}>*/}
                                {/*    <div className="alert alert-info mt-5" role="alert">*/}
                                {/*        <h4 className="alert-heading">Note:</h4>*/}
                                {/*        <p>Aww yeah, you successfully read this important alert message. This example*/}
                                {/*            text is going to run a bit longer so that you can see how spacing within an*/}
                                {/*            alert works with this kind of content.</p>*/}
                                {/*        <p className="mb-0">Whenever you need to, be sure to use margin utilities to*/}
                                {/*            keep*/}
                                {/*            things nice and tidy.</p>*/}
                                {/*    </div>*/}
                                {/*</div>*/}
                            </div>
                        </React.Fragment>
                    )
                }

            </div>
        </Layout>
    );
}

export default TransactionDetailsPage;
