import React from 'react';
import useSWR from 'swr';
import {toast} from "react-toastify";
import {useForm} from "@mantine/form";

import Layout from "../../components/Container/Layout";
import {pageMetaOptions} from "../../data";
import {confirmTransferOtp, initiateTransfer, resendTransferOTP} from "../../services/transfers";
import {useUserNotifications} from "../../@hooks/use-notification";
import {injectStyle} from "../../@helper/validations";

const metaOption = {
    ...pageMetaOptions,
    title: 'Transfer'
};

function TransferPage() {
    const {data: userProfileData} = useSWR('api/user-profile');
    const {data: userTransferData, mutate} = useSWR('api/transfer/user-incomplete-transfer');
    const {mutate: refreshUserNotification} = useUserNotifications();

    const balance = userProfileData?.stats?.accsDetails?.balance;
    const pendingTransfers = userTransferData?.data?.data || [];

    const form = useForm({
        initialValues: {
            payeeName: '',
            payeeBankName: '',
            payeeAccountNumber: '',
            payeeSortcode: '',
            amount: 0
        },
        validate: {
            payeeName: (val) => (!val ? 'Receiver\'s name is required' : null),
            payeeBankName: (val) => (!val ? 'Receiver\'s bank name is required.' : null),
            payeeAccountNumber: (val) => (!val ? 'Receiver\'s bank account number is required.' : null),
            payeeSortcode: (val) => (!val ? 'Receiver\'s bank sortcode is required.' : null),
            amount: (val) => ((!val || val > balance) ? `Entered amount should not be less than 1 and not greater than ${balance.toLocaleString()}` : null),
        }
    });

    const otpForm = useForm({
        initialValues: {
            otp: ''
        },
        validate: {
            otp: (val) => (!val ? 'OTP is required.' : null)
        }
    });

    // const injectStyle = (addStyle = {}) => ({
    //     border: '1px solid red',
    //     ...addStyle
    // });

    const onSubmitTransfer = async (formValue) => {
        form.validate();

        const {error} = await initiateTransfer(formValue);
        if (error)
            return toast.error(error.message, {autoClose: false});

        form.reset();
        toast.success('Transfer initiated.');
        await mutate();
        await refreshUserNotification();
    };

    const onConfirmOTP = async () => {
        otpForm.validate();
        const {error} = await confirmTransferOtp(otpForm.values);
        if (error)
            return toast.error(error.message, {autoClose: false});

        otpForm.reset();
        toast.success('Transfer confirmed.');
        await mutate();
        await refreshUserNotification();
    };

    const onResendOTP = async () => {
        let transferId;

        if(!!pendingTransfers?.length) {
            const transfer = pendingTransfers[0];
            transferId = transfer.id;
        }

        if (!transferId) return;

        const {error} = await resendTransferOTP(transferId);
        if (error)
            return toast.error(error.message, {autoClose: false});
        toast.success('Transfer OTP resent.');
    };

    const disableTransferInputs = !!pendingTransfers?.length || false;

    return (
        <Layout {...metaOption}>
            <div className="element-wrapper compact pt-4">
                <h6 className="element-header">Transfer</h6>
                <div className="element-box-tp">
                    <div className="row">
                        <div className="col-12 col-md-9 col-xl-6">
                            <form noValidate onSubmit={form.onSubmit(onSubmitTransfer)}>
                                <h5 className="form-header">Transfer Fund</h5>
                                {!!pendingTransfers?.length ? (
                                    <div className="form-desc">
                                        You have a pending transfer. Please enter the otp sent to your email to complete
                                        the transfer.
                                    </div>
                                ) : null}
                                <div className="form-group row">
                                    <label className="col-form-label col-sm-4" htmlFor="payeeName">Receiver Name</label>
                                    <div className="col-sm-8">
                                        <input
                                            className="form-control"
                                            disabled={disableTransferInputs}
                                            id="payeeName"
                                            placeholder="Enter receiver name"
                                            type="text"
                                            style={form.errors?.payeeName ? injectStyle() : {}}
                                            {...form.getInputProps('payeeName')}
                                        />
                                        {form.errors?.payeeName ? (<small className="text-danger">{form.errors?.payeeName}</small>) : null}
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-form-label col-sm-4" htmlFor="payeeBankName">Receiving Account
                                        Name</label>
                                    <div className="col-sm-8">
                                        <input
                                            className="form-control"
                                            disabled={disableTransferInputs}
                                            id="payeeBankName"
                                            placeholder="Receiving Account Name"
                                            type="text"
                                            style={form.errors?.payeeBankName ? injectStyle() : {}}
                                            {...form.getInputProps('payeeBankName')}
                                        />
                                        {form.errors?.payeeBankName ? (<small className="text-danger">{form.errors?.payeeBankName}</small>) : null}
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-form-label col-sm-4" htmlFor="payeeAccountNumber">Receiving
                                        Account Number</label>
                                    <div className="col-sm-8">
                                        <input
                                            className="form-control"
                                            disabled={disableTransferInputs}
                                            id="payeeAccountNumber"
                                            placeholder="Receiving Account Number"
                                            type="text"
                                            style={form.errors?.payeeAccountNumber ? injectStyle() : {}}
                                            {...form.getInputProps('payeeAccountNumber')}
                                        />
                                        {form.errors?.payeeAccountNumber ? (<small className="text-danger">{form.errors?.payeeAccountNumber}</small>) : null}
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-form-label col-sm-4" htmlFor="payeeSortcode">Receiving Account
                                        Sortcode</label>
                                    <div className="col-sm-8">
                                        <input
                                            className="form-control"
                                            disabled={disableTransferInputs}
                                            id="payeeSortcode"
                                            placeholder="Receiving Account Sortcode"
                                            type="text"
                                            style={form.errors?.payeeSortcode ? injectStyle() : {}}
                                            {...form.getInputProps('payeeSortcode')}
                                        />
                                        {form.errors?.payeeSortcode ? (<small className="text-danger">{form.errors?.payeeSortcode}</small>) : null}
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-form-label col-sm-4" htmlFor="amount">Amount</label>
                                    <div className="col-sm-8">
                                        <input
                                            className="form-control"
                                            disabled={disableTransferInputs}
                                            id="amount"
                                            placeholder="Amount"
                                            type="number"
                                            style={form.errors?.amount ? injectStyle() : {}}
                                            {...form.getInputProps('amount')}
                                        />
                                        {form.errors?.amount ? (<small className="text-danger">{form.errors?.amount}</small>) : null}
                                    </div>
                                </div>
                                <div className="form-buttons-w">
                                    <button className="btn btn-primary" type="submit" disabled={disableTransferInputs}>
                                        Submit
                                    </button>
                                </div>
                            </form>
                        </div>
                        {!!pendingTransfers?.length ? (
                            <div className="col-12 col-md-3 col-xl-6">
                                <form onSubmit={otpForm.onSubmit(onConfirmOTP)} noValidate>
                                    <h5 className="form-header">Confirm OTP</h5>
                                    <div className="form-desc">&nbsp;</div>
                                    <div className="bg-white py-4 px-3 rounded-3">
                                        <div className="row">
                                            <div className="col-sm-6">
                                                <div className="form-group row align-items-center">
                                                    <label className="col-sm-4" htmlFor="otp">Enter Otp</label>
                                                    <div className="col-sm-8">
                                                        <input
                                                            className="form-control"
                                                            id="otp"
                                                            placeholder="OTP"
                                                            type="text"
                                                            style={otpForm.errors?.otp ? injectStyle() : {}}
                                                            {...otpForm.getInputProps('otp')}
                                                        />
                                                        {otpForm.errors?.otp ? <small className="text-danger">{otpForm.errors?.otp}</small> : null}
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-sm-6">
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-sm-6">
                                                <div className="row">
                                                    <div className="col-sm-8 offset-sm-4">
                                                        <button type="submit" className="btn btn-primary">Confirm</button>
                                                        <button type="button" className="btn btn-link" onClick={onResendOTP}>Resend</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        ) : null}
                    </div>
                </div>
            </div>
        </Layout>
    );
}

export default TransferPage;
