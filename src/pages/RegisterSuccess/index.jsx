import React from 'react';
import {useNavigate} from "react-router-dom";
import {Helmet} from "react-helmet";

import {useLogo} from "../../@hooks/use-logo";

function RegisterSuccess() {
    const navigate = useNavigate();
    const Logo = useLogo('blue');

    return (

        <React.Fragment>
            <Helmet>
                <body className={'all-wrapper menu-side with-pattern'}/>
            </Helmet>

            <div className="all-wrapper menu-side with-pattern mt-4">
                <div className="auth-box-w pb-4">
                    <div className="logo-w">
                        <a href={process.env.REACT_APP_API_URL}>
                            <img alt="" src={Logo} width="100"/>
                        </a>
                    </div>
                    <h4 className="auth-header">Registration Completed</h4>

                    <div style={{
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                    }}>
                        <h6 className="element-header">Account created successfully.</h6>
                        <div className="element-content">
                            <p>Check you inbox for your account number and login password</p>
                        </div>
                        <div className="element-actions mt-3">
                            <button className="btn btn-primary btn-rounded"
                                    onClick={() => navigate('/login')}
                            >
                                Login
                            </button>
                        </div>
                    </div>

                </div>
            </div>
        </React.Fragment>
    );
}

export default RegisterSuccess;
