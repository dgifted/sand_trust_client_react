import React, {useMemo} from 'react';
import useSWR from "swr";
import {useNavigate} from "react-router-dom";


import {pageMetaOptions} from "../../data";
import Layout from "../../components/Container/Layout";
import Currency from "../../components/Currency";
import DataTableBase from "../../components/Container/DataTableBase";

const metaOptions = {
    ...pageMetaOptions,
    title: 'Transfer history'
};

function Transfers() {
    const {data: userTransfersData} = useSWR('api/transfer/user-transfers');
    const navigate = useNavigate();
    const transfers = userTransfersData?.data?.data || [];

    const userTransfersTableData = useMemo(() => {
        if (!transfers?.length)
            return [];

        return transfers.map(transfer => ({
            id: transfer.ref_id,
            recipient: transfer.payee_name,
            date: new Date(transfer.created_at).toLocaleDateString(),
            amount: (<div><Currency/>{transfer.amount}</div>),
            status: (<span className={`badge badge-${transfer.status_name === 'completed' ? 'success' : 'danger'}`}>{transfer.status_name}</span>),
        }));
    }, [transfers]);

    const tableColsRef = React.useRef([
        {name: 'Recipient', selector: row => row.recipient},
        {name: 'Date', selector: row => row.date},
        {name: 'Amount', selector: row => row.amount},
        {name: 'Status', selector: row => row.status},
        {
            cell: (transfer) => (
                <button
                    className={'btn btn-primary btn-sm text-capitalize'}
                    onClick={() => onViewTransferDetails(transfer.id)}>View detail</button>
            ),
            ignoreRowClick: true,
            allowOverflow: true,
            button: true,
        }
    ]);

    const onViewTransferDetails = (transferId) => navigate(`/transactions/${transferId}?type=transfer`);

    return (
        <Layout {...metaOptions}>
            <div className="element-wrapper">
                <div className="element-actions">
                    <button className="btn btn-sm btn-primary btn-rounded"
                            onClick={() => navigate('/transfers/new')}
                    >
                        Make a transfer
                    </button>
                </div>
                <h6 className="element-header">Transfer History</h6>
                <div className="element-content">
                    <div className="tablo-with-chart">
                        <div className="table-responsive">
                            <DataTableBase
                                columns={tableColsRef.current}
                                data={userTransfersTableData}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    );
}

export default Transfers;
