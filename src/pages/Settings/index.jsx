import React from 'react';
import Select from 'react-select';
import {MdOutlineChangeCircle} from "react-icons/md";
import {toast} from "react-toastify";
import {useForm} from "@mantine/form";
import {Switch} from "@mantine/core";

import Layout from "../../components/Container/Layout";
import {pageMetaOptions} from "../../data";
import {useAuthUser} from "../../@hooks/use-auth-user";
import {updateUserProfile, uploadProfilePicture} from "../../services/profiles";
import {useUserPreference} from "../../@hooks/use-preference";
import {updateUserLang, updateUserNotification, updateUserTheme} from "../../services/preferences";


const metaOption = {
    ...pageMetaOptions,
    title: 'Settings'
};

const langOptions = [
    {value: 'english', label: 'English'},
    {value: 'french', label: 'French'},
    {value: 'spanish', label: 'Spanish'},
];

function SettingsPage() {
    const {data: userData, mutate} = useAuthUser();
    const {data: userPrefData, mutate: refreshPreference} = useUserPreference();
    const [uploading, setUploading] = React.useState(false);
    const [uploadProgress, setUploadProgress] = React.useState(0);
    const user = userData?.data;
    const userPref = userPrefData?.data;
    const choiceLang = langOptions.find(lang => lang.value === userPref?.lang.toLowerCase());
    const [selectedLanguage, setSelectedLanguage] = React.useState(langOptions[0]);


    const form = useForm({
        initialValues: {
            firstName: user?.first_name,
            lastName: user?.last_name,
            dob: user?.dob,
            username: user?.username,
        }
    });

    const photoRef = React.useRef();

    const onUploadProgress = ({loaded, total}) => {
        let timer;
        if (loaded < total)
            setUploading(true);
        else {
            if (timer)
                clearTimeout(timer);

            timer = setTimeout(() => {
                setUploading(false);
                setUploadProgress(0);
            }, 1000);

            return;
        }

        const progress = Math.ceil((loaded / total) * 100);
        setUploadProgress(progress);
    }

    const onUploadImage = async (evt) => {
        const imageFile = evt.target.files[0];
        if (!imageFile) return;
        const formData = new FormData();
        formData.append('photo', imageFile);

        const {error} = uploadProfilePicture(formData, onUploadProgress);
        if (error)
            return toast.error(error.message);

        toast.success('Profile picture changed successfully.');
        await mutate();
    }

    const onUpdateProfile = async (value) => {
        const {error} = await updateUserProfile(value);

        if (error)
            return toast.error(error.message, {autoClose: false});
        await mutate();
        toast.success('Profile information updated.');
    };

    React.useEffect(() => {
        setSelectedLanguage(choiceLang);
    }, [choiceLang]);


    return (
        <Layout {...metaOption}>
            <div className="element-wrapper">
                <h6 className="element-header">Settings</h6>
                <div className="element-content">
                    <div className="content-box">
                        <div className="row">
                            <div className="col-sm-5">
                                <div className="user-profile compact" style={{position: 'relative'}}>
                                    {uploading ? (
                                        <div style={{
                                            height: '6px',
                                            backgroundColor: '#ff0099',
                                            width: `${uploadProgress}%`,
                                            position: "absolute",
                                            top: '0',
                                            left: '0',
                                            borderRadius: '3px'
                                        }}/>
                                    ) : null}
                                    <div className={'w-100 d-flex justify-content-center align-items-center pt-5'}
                                         style={{}}
                                    >
                                        <img
                                            src={user?.avatar}
                                            alt=""
                                            width={150}
                                            height={150}
                                            className={'rounded-circle'}
                                        />
                                    </div>
                                    <div className="up-controls">
                                        <div className="row">
                                            <div className="col-12 text-center">
                                                <input accept={'image/*'} className={'d-none'} type={'file'}
                                                       ref={photoRef} onChange={onUploadImage}/>
                                                <button className="btn btn-primary btn-sm"
                                                        onClick={() => photoRef.current.click()}>
                                                    <MdOutlineChangeCircle size={'1.2rem'}/>{' '}
                                                    <span>Change photo</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="element-wrapper">
                                    <div className="element-box">
                                        <h6 className="element-header">Preferences</h6>
                                        <div className={'d-flex flex-column'}>
                                            <div>
                                                <Switch
                                                    label={'Notifications'}
                                                    onLabel={'ON'}
                                                    offLabel={'OFF'}
                                                    size={'lg'}
                                                    checked={!!userPref?.notification_state}
                                                    onChange={async () => {
                                                        const {error} = await updateUserNotification();
                                                        if (error)
                                                            return toast.error(error.message)
                                                        toast.success('Notification changed.');
                                                        await refreshPreference();
                                                    }}
                                                />
                                            </div>

                                            <div className={'my-4'} style={{maxWidth: '150px'}}>
                                                <Select
                                                    value={selectedLanguage}
                                                    onChange={async (option) => {
                                                        const {error} = await updateUserLang({lang: option.value});
                                                        if (error)
                                                            return toast.error(error.message);
                                                        toast.success('Language option changed.')
                                                        await refreshPreference();
                                                        // setSelectedLanguage(choiceLang);
                                                    }}
                                                    options={langOptions}
                                                />
                                            </div>

                                            <Switch
                                                label={'Light/Dark (mode)'}
                                                onLabel={'LIT'}
                                                offLabel={'DARK'}
                                                size={'lg'}
                                                checked={!!userPref?.theme}
                                                onChange={async () => {
                                                    const {error} = await updateUserTheme();
                                                    if (error)
                                                        return toast.error(error.message);
                                                    toast.success('Theme mode changed');
                                                    await refreshPreference();
                                                }}
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-7">
                                <div className="element-wrapper">
                                    <div className="element-box">
                                        <form noValidate onSubmit={form.onSubmit(onUpdateProfile)}>
                                            <div className="element-info">
                                                <div className="element-info-with-icon">
                                                    <div className="element-info-text">
                                                        <h5 className="element-inner-header">Profile Information</h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor=""> Email address</label>
                                                <input className="form-control" type="email" defaultValue={user?.email}
                                                       readOnly/>
                                                <div className="help-block form-text with-errors form-control-feedback">
                                                </div>
                                            </div>

                                            <div className="row">
                                                <div className="col-sm-6">
                                                    <div className="form-group">
                                                        <label htmlFor="">First Name</label>
                                                        <input
                                                            className="form-control"
                                                            {...form.getInputProps('firstName')}
                                                        />
                                                    </div>
                                                </div>
                                                <div className="col-sm-6">
                                                    <div className="form-group">
                                                        <label htmlFor="">Last Name</label>
                                                        <input
                                                            className="form-control"
                                                            {...form.getInputProps('lastName')}
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-6">
                                                    <div className="form-group">
                                                        <label htmlFor="">Date of Birth</label>
                                                        <input
                                                            className="form-control"
                                                            {...form.getInputProps('dob')}
                                                        />
                                                    </div>
                                                </div>
                                                <div className="col-sm-6">
                                                    <div className="form-group">
                                                        <label htmlFor="">Username</label>
                                                        <div className="input-group">
                                                            <div className="input-group-prepend">
                                                                <div className="input-group-text">@</div>
                                                            </div>
                                                            <input
                                                                className="form-control"
                                                                placeholder="Enter username"
                                                                {...form.getInputProps('username')}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="form-buttons-w">
                                                <button className="btn btn-primary" type="submit">
                                                    Save changes
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    );
}

export default SettingsPage;
