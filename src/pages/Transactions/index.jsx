import React from 'react';
import {useNavigate} from "react-router-dom";
import useSWR from "swr";

import {pageMetaOptions} from "../../data";
import Layout from "../../components/Container/Layout";
import DataTableBase from "../../components/Container/DataTableBase";
import Currency from "../../components/Currency";

const metaOptions = {
    ...pageMetaOptions,
    title: 'Transaction history'
};

// const userTransactionsTableCols = ;

function TransactionsPage() {
    const {data: transactionData} = useSWR('api/transaction');
    const {data: userProfileData} = useSWR('api/user-profile');
    const navigate = useNavigate();

    const transactions = transactionData?.data?.data || [];
    const currency = userProfileData?.stats?.accsDetails?.currency;

    const onViewTransaction = (transId, type) => navigate(`/transactions/${transId}?type=${type}`)

    const userTransactionsTableData = React.useMemo(() => {
        if (!transactions.length)
            return [];

        return transactions.map(transaction => ({
            id: transaction.id,
            recipient: transaction.receiver,
            invoice: 'XXX-XXXX-XX',
            date: new Date(transaction.timeStamp).toLocaleDateString(),
            amount: (<div><Currency/>{transaction.amount}</div>),
            status: (<span className={`badge badge-${transaction.status == 'completed' ? 'success' : 'danger'}`}>{transaction.status}</span>),
            type: (<span className={`text-${transaction.type == 'deposit' ? 'success' : 'danger'}`}>{transaction.type}</span>)
        }));

    }, [transactions]);

    const tableColsRef = React.useRef([
        {name: 'Recipient', selector: row => row.recipient},
        {name: 'Invoice', selector: row => row.invoice},
        {name: 'Date', selector: row => row.date},
        {name: 'Amount', selector: row => row.amount},
        {name: 'Status', selector: row => row.status},
        {name: 'Type', selector: row => row.type},
        {
            cell: (transaction) => (
                <button
                    className={'btn btn-primary btn-sm text-capitalize'}
                    onClick={() => onViewTransaction(transaction.id, transaction.type.props.children)}>View detail</button>
            ),
            ignoreRowClick: true,
            allowOverflow: true,
            button: true,
        }
    ]);

    return (
        <Layout {...metaOptions}>
            <div className="element-wrapper">
                <h6 className="element-header">Transactions</h6>
                <div className="element-content">
                    <div className="tablo-with-chart">
                        <div className="table-responsive">
                            <DataTableBase
                                columns={tableColsRef.current}
                                data={userTransactionsTableData}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    );
}

export default TransactionsPage;
