import React from 'react';
import useSWR from 'swr';
import {CopyToClipboard} from "react-copy-to-clipboard";
import {toast} from "react-toastify";
import {useNavigate} from "react-router-dom";
import {useModals} from "@mantine/modals";

import {baseUrl} from "../api";
import Layout from "../components/Container/Layout";
import {pageMetaOptions} from "../data";
import Currency from "../components/Currency";
import EmptyList from "../components/UI/EmptyList";
import WithdrawalForm from "../components/WithdrawalForm";
import {initiateCreditLimitRequest} from "../services/profiles";
import {withDrawFromAccount} from "../services/banks";
import TransactionHistoryChart from "../components/TransactionHistoryChart";

function HomePage() {
    const {data: profileData, error} = useSWR('api/user-profile');
    const {data: transactionData} = useSWR('api/transaction');
    const {data: pendingCreditLimitRequestData} = useSWR('api/credit-limit/pending-request');
    const {data: localBankData} = useSWR('api/user-bank');
    const {mutate: refreshPendingWithdrawalRequest} = useSWR('api/user-withdrawal/pending');
    const siteUrl = baseUrl;
    const [textCopied, setTextCopied] = React.useState(false);
    const navigate = useNavigate();
    const modals = useModals();

    const onRefCopied = () => {
        setTextCopied(true);
        setTimeout(() => {
            setTextCopied(false);
        }, 2000);
    }

    if (error) {
        toast.error(error.message, {autoClose: 2000});
    }

    const userStats = profileData?.stats;
    const userRefId = profileData?.user?.uid;
    const transactions = transactionData?.data?.data;
    const balance = userStats?.accsDetails?.balance || 0;
    const pendingCreditLimitRequest = pendingCreditLimitRequestData?.data;
    const localBanks = localBankData?.data?.data;

    const confirmCreditIncreaseRequest = async (modalId) => {
        const {error} = await initiateCreditLimitRequest();
        if (error)
            return toast.error(error.message);

        modals.closeModal(modalId);
        toast.success('Request for credit limit increase sent.');
    };

    const onRequestCreditIncrease = () => {
        let modalId = modals.openModal({
            title: 'Request Credit Limit Increase',
            children: (
                <div className="d-flex flex-column justify-content-between align-items-lg-stretch">
                    <p>Confirm request for credit limit increment.</p>
                    <button className="btn btn-primary" onClick={() => confirmCreditIncreaseRequest(modalId)}>Confirm
                    </button>
                </div>
            ),
        });
    }

    const onWithdraw = async (val) => {
        const {error} = await withDrawFromAccount(val);
        if (error)
            return toast.error(error.message);
        toast.success('Withdrawal request sent. Being processed.');
        await refreshPendingWithdrawalRequest();
    }

    return (
        <Layout {...pageMetaOptions}>
            <div className="element-wrapper compact pt-4">
                <h6 className="element-header">Financial Overview</h6>
                <div className="element-box-tp">
                    <div className="row">
                        <div className="col-lg-7 col-xxl-6">
                            <div className="element-balances">
                                <div className="balance hidden-mobile">
                                    <div className="balance-title">Total Balance</div>
                                    <div className="balance-value">
                                            <span>
                                                <Currency/>
                                                {Number(balance ?? 0).toLocaleString()}
                                            </span>
                                    </div>
                                    <div className="balance-link">
                                        <button
                                            className="btn btn-link btn-underlined"
                                            onClick={() => navigate('/transactions')}
                                        >
                                            <span>View Statement</span>
                                            <i className="os-icon os-icon-arrow-right4"/>
                                        </button>
                                    </div>
                                </div>
                                <div className="balance">
                                    <div className="balance-title">Credit Available</div>
                                    <div className="balance-value">
                                        <Currency/>
                                        {userStats?.accsDetails?.creditLimit.toLocaleString()}
                                    </div>
                                    <div className="balance-link">
                                        <button
                                            className="btn btn-link btn-underlined"
                                            disabled={pendingCreditLimitRequest}
                                            onClick={onRequestCreditIncrease}
                                            type="button"
                                        >
                                            <span>{!!pendingCreditLimitRequest ? '1 Request pending' : 'Request Increase'}</span>
                                            <i className="os-icon os-icon-arrow-right4"/>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-5 col-xxl-6">
                            <div className="alert alert-warning borderless">
                                <h5 className="alert-heading">Refer Friends. Get Rewarded</h5>
                                <p>You can earn: 15,000 Membership Rewards points for each approved referral
                                    – up to 55,000 Membership Rewards points per calendar year.</p>
                                <div className="alert-btn">
                                    <CopyToClipboard
                                        text={`${siteUrl}/register?ref=${userRefId}`}
                                        onCopy={onRefCopied}
                                    >
                                        <button className="btn btn-white-gold">
                                            <i className={`os-icon os-icon-ui-92 ${textCopied ? 'text-success' : ''}`}/>
                                            {textCopied ? <span className="text-success">Link copied</span> :
                                                <span>Send Referral</span>}
                                        </button>
                                    </CopyToClipboard>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-7 col-xxl-6">
                    <div className="element-wrapper">
                        <TransactionHistoryChart
                            transactionData={transactions}
                        />
                    </div>
                </div>
                <div className="col-lg-5 col-xxl-6">
                    <div className="element-wrapper">
                        <div className="element-box">
                            <WithdrawalForm
                                onSubmit={async (val) => await onWithdraw(val)}
                                balance={balance}
                                banks={localBanks?.length ? localBanks : []}
                            />
                        </div>
                    </div>
                </div>
            </div>
            <div className="element-wrapper">
                <h6 className="element-header">Recent Transactions</h6>
                <div className="element-box-tp">
                    <div className="table-responsive">
                        <table className="table table-padded">
                            <thead>
                            <tr>
                                <th>Status</th>
                                <th>Date</th>
                                <th>Description</th>
                                <th className="text-center">Category</th>
                                <th className="text-right">Amount</th>
                            </tr>
                            </thead>
                            <tbody>
                            {!!transactions?.length
                                ? transactions.map(transaction => {
                                    let statusColor;
                                    switch (transaction.status) {
                                        case 'completed':
                                            statusColor = 'green';
                                            break;
                                        case 'pending':
                                            statusColor = 'yellow';
                                            break;
                                        case 'failed':
                                            statusColor = 'red';
                                            break;
                                    }

                                    return (
                                        <tr key={transaction.id}>
                                            <td className="nowrap">
                                                <span className={`status-pill smaller ${statusColor}`}/>
                                                <span>{transaction.status}</span>
                                            </td>
                                            <td>
                                                <span>{transaction.createdOn}</span>
                                                <span className="smaller lighter">{transaction.createdTime}</span>
                                            </td>
                                            <td className="cell-with-media">
                                                    <span>
                                                        {transaction.type === 'deposit' ? 'Account top/Deposit' : 'Money Transfer'}
                                                    </span>
                                            </td>
                                            <td className="text-center">
                                                <a
                                                    className={`badge badge-${transaction.type === 'deposit' ? 'success' : 'danger'}`}
                                                    href="#">{transaction.type === 'deposit' ? 'Credit' : 'Debit'}
                                                </a>
                                            </td>
                                            <td className="text-right bolder nowrap">
                                                    <span
                                                        className={`${transaction.type === 'deposit' ? 'text-success' : 'text-danger'}`}>
                                                        {`${transaction.type === 'deposit' ? '+' : '-'}`}
                                                        <Currency/>{transaction.amount.toLocaleString()}
                                                    </span>
                                            </td>
                                        </tr>
                                    );
                                })
                                : <tr>
                                    <td className={'text-center'} colSpan={5}>
                                        <EmptyList message="No transfer history"/>
                                    </td>
                                </tr>
                            }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </Layout>
    );
}

export default HomePage;
