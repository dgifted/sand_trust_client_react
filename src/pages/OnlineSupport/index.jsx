import React from 'react';
import {toast} from "react-toastify";

import {pageMetaOptions} from "../../data";
import Layout from "../../components/Container/Layout";
import {useUserSupports} from "../../@hooks/use-support";
import EmptyList from "../../components/UI/EmptyList";
import SupportTicketItem from "../../components/SupportTicketItem";
import {useAuthUser} from "../../@hooks/use-auth-user";
import {createSupportTicket, getSupportTicketReplies, postSupportTicketReply} from "../../services/support-tickets";
import SupportTicketReplyItem from "../../components/SupportTicketReplyItem";
import TicketReplyBox from "../../components/TicketReply";

const metaOption = {
    ...pageMetaOptions,
    title: 'Support'
};

function OnlineSupportPage() {
    const {data: userSupportData, mutate: refreshTickets} = useUserSupports();
    const {data: userData} = useAuthUser();
    const userSupports = userSupportData?.data?.data;
    const user = userData?.data;
    const [tickets, setTickets] = React.useState([]);
    const [selectedIndex, setSelectedIndex] = React.useState(0);
    const [supportReplies, setSupportReplies] = React.useState([]);
    const [filter, setFilter] = React.useState('all');
    const [editorOpen, toggleEditor] = React.useState({
        new: false,
        reply: false
    });

    React.useEffect(() => {
        if (!!userSupports?.length)
            setTickets(userSupports);
    }, [userSupports]);

    // const activeTicket = tickets[selectedIndex];
    let activeTicket;
    if (!!tickets?.length)
        activeTicket = tickets[selectedIndex];

    const fetchActiveSupportReplies = React.useCallback(async () => {
        if (!activeTicket) {
            setSupportReplies([]);
            return;
        }

        const {error, result} = await getSupportTicketReplies(activeTicket?.id);
        if (error)
            return toast.error(
                'We could not load support messages at this time please try again. '
            );
        setSupportReplies(result);
    }, [activeTicket]);

    React.useEffect(() => {
        fetchActiveSupportReplies();
    }, [fetchActiveSupportReplies]);

    const onSupportLoad = (idx) => setSelectedIndex(idx);

    const onPostCreate = async (payload) => {
        const {error} = await createSupportTicket(payload);

        if (error)
            return toast.error(error.message);

        toast.success('Support ticket created successfully');
        await refreshTickets();
    };

    const onPostReply = async ({content}) => {
        const {error} = await postSupportTicketReply({content}, activeTicket.id);
        if (error)
            return toast.error(error.message);

        const {error: refetchError, result} = await getSupportTicketReplies(activeTicket.id);
        if (!refetchError)
            setSupportReplies(result);
    }

    const onFilterByStatus = React.useCallback(() => {
        setTickets(userSupports);

        setTimeout(() => {
            if (!!tickets?.length) {
                let filteredTickets;
                if (filter === 'closed')
                    filteredTickets = tickets.filter(item => Number(item.is_closed) === 2);
                if (filter === 'open')
                    filteredTickets = tickets.filter(item => Number(item.is_closed) === 1);
                if (filter === 'pending')
                    filteredTickets = tickets.filter(item => (Number(item.is_closed) === 1 && Number(item.reply_count) === 0));
                if (filter === 'all')
                    filteredTickets = userSupports;

                setSelectedIndex(0);
                setTickets(filteredTickets);
            }
        }, 100);

    }, [filter]);

    React.useEffect(() => {
        onFilterByStatus()
    }, [onFilterByStatus]);

    return (
        <Layout {...metaOption}>
            <div className="element-wrapper">
                <h6 className="element-header">Support</h6>
                <div className="element-content">
                    <div className="content-box">
                        <div className="support-index">
                            <div className="support-tickets">
                                <div className="support-tickets-header">
                                    <div className="tickets-control">
                                        <h5>Tickets</h5>
                                        <div className="element-search"><input placeholder="Type to filter tickets..."/>
                                        </div>
                                    </div>
                                    <div className="tickets-filter">
                                        <div className="form-group mr-3">
                                            <label className="d-none d-md-inline-block mr-2">Status</label>
                                            <select className="form-control-sm" onChange={e => setFilter(e.target.value)} value={filter}>
                                                <option value="all">All</option>
                                                <option value="closed">Closed</option>
                                                <option value="open">Open</option>
                                                <option value="pending">Pending</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <TicketReplyBox
                                    showTitleField
                                    openText="New ticket"
                                    onSubmit={onPostCreate}
                                    style={{minHeight: '300px'}}
                                />
                                {!!tickets?.length ? (
                                    <>
                                        {tickets?.map((ticket, idx) => (
                                            <SupportTicketItem
                                                key={ticket.id.toString()}
                                                title={ticket.title}
                                                excerpt={ticket.excerpt}
                                                status={Number(ticket.is_closed) === 1 ? 'open' : 'closed'}
                                                author={`${user?.first_name} ${user?.last_name}`}
                                                avatar={user?.avatar}
                                                isActive={selectedIndex === idx}
                                                onclick={() => onSupportLoad(idx)}
                                                updatedLast={new Date(ticket.updated_at).toDateString()}
                                            />
                                        ))}
                                    </>
                                ) : (
                                    <EmptyList message={'You do not have any support ticket.'}/>
                                )}
                                <div className="load-more-tickets">
                                    <a href="#"><span>Load More Tickets...</span></a>
                                </div>
                            </div>
                            <div className="support-ticket-content-w">
                                <div className="support-ticket-content">
                                    <div className="support-ticket-content-header">
                                        <h3 className="ticket-header">{activeTicket?.title}</h3>
                                        <a className="back-to-index" href="#">
                                            <i className="os-icon os-icon-arrow-left5"/>
                                            <span>Back</span>
                                        </a>
                                        <a className="show-ticket-info" href="#">
                                            <span>Show Details</span>
                                            <i className="os-icon os-icon-documents-03"/>
                                        </a>
                                    </div>
                                    <div className="ticket-thread">
                                        {!!supportReplies.length ? (
                                            <>
                                                {supportReplies.map(reply => (
                                                    <SupportTicketReplyItem
                                                        key={reply.id}
                                                        author={`${reply.author.first_name} ${reply.author.last_name}`}
                                                        content={reply.content}
                                                        authorAvatar={reply.author.avatar}
                                                        isSelf={Number(reply.author.id) === (user?.id)}
                                                        repliedOn={new Date(reply.created_at).toDateString()}
                                                    />
                                                ))}
                                            </>
                                        ) : (
                                            <EmptyList message={'Support ticket do not have replies yet.'}/>
                                        )}
                                        {activeTicket?.is_closed !== 1 ? null : (
                                            <TicketReplyBox onSubmit={onPostReply} openText="Reply"/>
                                        )}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    );
}

export default OnlineSupportPage;
