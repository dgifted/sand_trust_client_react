import {
    updateUserLangPreference,
    updateUserNotificationPreference,
    updateUserThemePreference
} from "../api/preferences";

export const updateUserLang = async (payload) => {
    try {
        await updateUserLangPreference(payload);
        return {error: null};
    } catch (e) {
        return {error: e};
    }
};

export const updateUserNotification = async () => {
    try {
        await updateUserNotificationPreference();
        return {error: null};
    } catch (e) {
        return {error: e};
    }
}

export const updateUserTheme = async () => {
    try {
        await updateUserThemePreference();
        return {error: null};
    } catch (e) {
        return {error: e};
    }
};
