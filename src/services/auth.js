import {callAPI} from "../api";

export const authService = {
    signIn: async (credentials) => await callAPI('api/auth/login', 'post', credentials),
    signOut: async () => await callAPI('api/auth/logout', 'post'),
    signUp: async  (formPayload) => await callAPI('api/auth/register', 'post', formPayload),
    forgotPassword: async (payload) => await callAPI('api/auth/forgot-password', 'post', payload),
    resetPassword: async (payload, token) => await callAPI(`api/auth/reset-password/${token}`, 'post', payload)
};
