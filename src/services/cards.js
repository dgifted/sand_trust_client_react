import {activateCard, deactivateCard} from "../api/cards";

export const activateUserCard = async (cardId) => {
    try {
        const resp = await activateCard(cardId);
        return {error: null, result: resp};
    } catch (e) {
        return {error: e, result: null};
    }
}

export const deactivateUserCard = async (cardId) => {
    try {
        const resp = await deactivateCard(cardId);
        return {error: null, result: resp};
    } catch (e) {
        return {error: e, result: null};
    }
}
