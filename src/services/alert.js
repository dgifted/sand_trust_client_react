import Swal from 'sweetalert2';
import withReactContent from "sweetalert2-react-content";

const Alert = withReactContent(Swal);

export const alertService = async ({title, message, icon = 'success'}) => {
    await Alert.fire({
        title,
        html: message,
        icon
    });
}
