import {createSupportTicket as createNewTicket, getTicketReplies, postTicketReply} from "../api/support-tickets";

export const getSupportTicketReplies = async (ticketId) => {
    try {
        const resp = await getTicketReplies(ticketId);
        return {error: null, result: resp.data.data};
    } catch (e) {
        return {error: e, result: null};
    }
};

export const createSupportTicket = async (payload) => {
    try {
        await createNewTicket(payload);
        return {error: null};
    } catch (e) {
        return {error: e.message};
    }
};

export const postSupportTicketReply = async (payload, ticketId) => {
    try {
        const resp = await postTicketReply(payload, ticketId);
        return {error: null, result: resp.data}
    } catch (e) {
        return {error: e, result: null};
    }
};
