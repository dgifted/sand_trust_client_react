import {postLocalBank, sendWithdrawalRequest} from "../api/banks";

export const postLocalBankLink = async (payload) => {
    try {
        await postLocalBank(payload);
        return {error: null}
    } catch (e) {
        return {error: e}
    }
}

export const withDrawFromAccount = async (payload) => {
    try {
        await sendWithdrawalRequest(payload);
        return {error: null}
    } catch (e) {
        return {error: e}
    }
}
