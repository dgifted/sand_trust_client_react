export const cardNumberFormat = (cardNumberRaw) => {
    const numSplitted = cardNumberRaw.split('');
    const buffer = [];

    for (let i = 0; i < numSplitted.length + 3; i++) {
        if (i == 4 || i == 8 || i == 12) {
            buffer.push(` ${numSplitted[i]}`);
        } else {
            buffer.push(numSplitted[i]);
        }

    }

    return buffer.join('');
}


export const maskCardNumber = (cardNumber) => {
    const numArr = cardNumber.split(' ');
    return '**** **** **** ' +numArr[numArr.length - 1];
}
