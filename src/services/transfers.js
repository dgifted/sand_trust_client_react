import {postTransfer, confirmTransferOtp as confirmOtp, resendTransferOtp as resendOtp} from "../api/transfers";

export const initiateTransfer = async (payload) => {
    try {
        const result = await postTransfer(payload);
        return {error: null, result: result}
    } catch (e) {
        return {error: e, result: null};
    }
};

export const confirmTransferOtp = async (payload) => {
    try {
        const resp = await confirmOtp(payload);
        return {error: null, result: resp};
    } catch (e) {
        return {error: e, result: null};
    }
};

export const resendTransferOTP = async (transferId) => {
    try {
        const resp = await resendOtp(transferId);
        return {error: null, result: resp};
    } catch (e) {
        return {error: e, result: null};
    }
}
