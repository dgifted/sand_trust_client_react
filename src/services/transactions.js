import {getAllTransaction, getSingleTransaction} from "../api/transactions";

export const getAllUserTransactions = async () => {
    try {
        const resp = await getAllTransaction();
        return {error: null, result: resp}
    } catch (e) {
        return {error: e, result: null}
    }
}

export const getUserSingleTransaction = async (transId, type = 'deposit') => {
    try {
        const resp = await getSingleTransaction(transId, type);
        return {error: null, result: resp}
    } catch (e) {
        return {error: e, result: null}
    }
}
