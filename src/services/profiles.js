import {
    uploadProfilePicture as pictureUpload,
    updateUserProfile as profileDataUpdate,
    requestCreditIncrease
} from "../api/profiles";

export const uploadProfilePicture = async (payload, onUploadProgress) => {

    try {
        const resp = await pictureUpload(payload, onUploadProgress);
        return {error: null, result: resp};
    } catch (e) {
        return {error: e, result: null};
    }
};

export const updateUserProfile = async (payload) => {
    try {
        const resp = await profileDataUpdate(payload);
        return {error: null, result: resp};
    } catch (e) {
        return {error: e, result: null};
    }
};

export const initiateCreditLimitRequest = async () => {
    try {
        await requestCreditIncrease();
        return {error: null};
    } catch (e) {
        return {error: e};
    }
}
