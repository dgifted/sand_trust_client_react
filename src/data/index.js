export const pageMetaOptions = {
    description: 'Slick Dashboard for bank customers',
    siteName: process.env.REACT_APP_SITE_NAME,
    title: 'Home page'
}
