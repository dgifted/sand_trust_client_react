export const slickOption = {
    dots: true,
    infinite: true,
    adaptiveHeight: false,
    slidesToShow: 1,
    slidesToScroll: 1,
};
