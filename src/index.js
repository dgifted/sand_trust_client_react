import React from 'react';
import ReactDOM from 'react-dom/client';
import {BrowserRouter as Router} from 'react-router-dom';
import {SWRConfig} from 'swr';
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';


import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {callAPI} from "./api";
import {AppContexts} from "./contexts";

export const fetcher = (url) => callAPI(url);

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <React.StrictMode>
        <Router>
            <SWRConfig
                value={{
                    fetcher
                }}
            >
                <AppContexts>
                    <App/>
                    <ToastContainer />
                </AppContexts>
            </SWRConfig>
        </Router>
    </React.StrictMode>
);
reportWebVitals();
