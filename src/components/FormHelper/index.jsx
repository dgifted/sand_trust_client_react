import React from "react";

export function RequiredMarker() {
    return <span className="text-danger" style={{fontSize: '1.3rem'}}>*</span>;
}

export function ErrorMessage({message}) {
    return <i className="text-danger">{message}</i>;
}
