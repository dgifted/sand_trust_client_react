import React from 'react';
import Select from 'react-select';

function CardRequestModal({modalRef, open, close, cardTypeOptions = []}) {
    const [selectedOption, setSelectedOption] = React.useState(null);
    let id;

    const closeHandler = (refId) => close(id, selectedOption);
    open(modalContent);

    const modalContent = () => {
        id = modalRef.openModal({
            title: 'Card Request',
            children: (
                <React.Fragment>
                    <label className={'text-capitalize'}>Select card type</label>
                    <Select
                        defaultValue={selectedOption}
                        onChange={setSelectedOption}
                        option={cardTypeOptions}
                    />
                </React.Fragment>
            )
        });
    }

    return null;
}

export default CardRequestModal;
