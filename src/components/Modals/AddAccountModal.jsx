import React from 'react';
import {useForm} from "@mantine/form";
import {injectStyle} from "../../@helper/validations";

function AddAccountModal({modalRef, modalId, onSubmit}) {

    const addBankForm = useForm({
        initialValues: {
            bankName: '',
            bankAccountNumber: ''
        },
        validate: {
            bankName: (val) => (!val ? 'Bank name is required' : null),
            bankAccountNumber: (val) => (!val ? 'Bank account number is required' : null)
        }
    });

    const onSubmitHandler = (val) => {
        addBankForm.validate();
        onSubmit(val)
            .then(() => modalRef.closeModal(modalId));
    }

    console.log('addBankForm errors: ', addBankForm.errors);

    return (
        <form noValidate onSubmit={addBankForm.onSubmit(onSubmitHandler)}>
            <div className="d-flex flex-column align-items-center" style={{flexGaps: '10px'}}>
                <input
                    className="form-control"
                    placeholder="Enter bank name"
                    style={addBankForm.errors?.bankName ? injectStyle() : {}}
                    {...addBankForm.getInputProps('bankName')}
                />
                {addBankForm.errors?.bankName ?
                    <small className="text-danger" style={{marginTop: '-10px'}}>
                        {addBankForm.errors?.bankName}
                    </small> : null}
                <input
                    className="form-control my-3"
                    placeholder="Enter bank account number"
                    style={addBankForm.errors?.bankAccountNumber ? injectStyle() : {}}
                    {...addBankForm.getInputProps('bankAccountNumber')}
                />
                {addBankForm.errors?.bankAccountNumber ?
                    <small className="text-danger" style={{marginTop: '-10px'}}>
                        {addBankForm.errors?.bankAccountNumber}
                    </small> : null}
            </div>
            <div className="d-flex justify-content-end">
                <button className="btn btn-primary mr-2" type="submit">Add</button>
                <button
                    className="btn btn-danger"
                    onClick={() => modalRef.closeModal(modalId)}
                    type="button">Cancel
                </button>
            </div>
        </form>
    );
}

export default AddAccountModal;
