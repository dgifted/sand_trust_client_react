import React from 'react';

function SupportTicketReplyItem({author, authorAvatar, content, isSelf, repliedOn}) {

    return (
        <div className={`ticket-reply ${isSelf ? 'highlight' : ''}`}>
            <div className={`ticket-reply-info`}>
                <a className="author with-avatar" href="#">
                    <img alt="" src={authorAvatar}/>
                    <span>{author}</span>
                </a>
                <span className="info-data">
                    <span className="label">replied on</span>
                    <span className="value">{repliedOn}</span>
                </span>
            </div>
            <div className="ticket-reply-content">
                <div dangerouslySetInnerHTML={{__html: content}} />
            </div>
        </div>
    );
}

export default SupportTicketReplyItem;
