import React from 'react';

function NotificationAvatar({title, variantCode}) {
    const initial = title.split('')[0];

    let variant;

    switch (Number(variantCode)) {
        case 0:
            variant = 'primary';
            break;
        case 1:
            variant = 'info';
            break;
        case 2:
            variant = 'danger';
            break;
        case 3:
            variant = 'warning';
            break;
    }

    return (
        <div
            className={`bg-${variant} text-uppercase`}
            style={{
                height: '35px',
                width: '35px',
                borderRadius: '50%',
                display: 'flex',
                justifyContent: "center",
                alignItems: 'center',
                fontSize: '1.0rem',
                fontWeight: 'bold'
            }}
        >
            {initial}
        </div>
    );
}

export default NotificationAvatar;
