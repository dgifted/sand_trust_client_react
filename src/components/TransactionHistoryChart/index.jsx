import React from 'react';
import {LineChart, Line} from 'recharts';


function TransactionHistoryChart({transactionData = []}) {
    console.log('transactionData -> ', transactionData)
    return (
        <div className="element-box">
            <div className="element-actions">
                <div className="form-group">
                    <select className="form-control form-control-sm">
                        <option selected="true">Last 30 days</option>
                        <option>This Week</option>
                        <option>This Month</option>
                        <option>Today</option>
                    </select>
                </div>
            </div>
            <h5 className="element-box-header">Transaction History</h5>
            <hr/>
            <div className="el-chart-w">
                    <LineChart width={400} height={150} data={transactionData}>
                        <Line
                            dataKey="amount"
                            stroke="#8884d8"
                            strokeWidth={2}
                            type="monotone"
                        />
                    </LineChart>
            </div>
        </div>
    );
}

export default TransactionHistoryChart;
