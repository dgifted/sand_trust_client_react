import React from 'react';
import {Link} from 'react-router-dom'

function Navigate({link}) {
    return (
        <Link to={link}/>
    );
}

export default Navigate;
