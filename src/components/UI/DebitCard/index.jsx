import React from 'react';

import Logo from '../../../assets/images/logo.png';
import classes from './DebitCard.module.css';
import {cardNumberFormat, maskCardNumber} from "../../../services/card-number-format";

function DebitCard({number, balance, holderName, validTru, currency, blocked}) {
    return (
        <div className={`${classes.cardContainer} ${blocked ? classes.blocked : ''}`}>
            {blocked ? <span className={`text-danger text-uppercase ${classes.blockedText}`}>Blocked</span> : null}
            <div className={classes.cardBalanceSummary}>
                <div>
                    <small>Balance</small>
                    <h3><em dangerouslySetInnerHTML={{__html: currency}}/>{balance?.toLocaleString()}</h3>
                </div>
                <img src={Logo} alt={''} style={{maxWidth: '100px', maxHeight: '40px'}}/>
            </div>

            <div className={`my-4 ${classes.cardNumber}`}>
                <em>{maskCardNumber(cardNumberFormat(number))}</em>
            </div>

            <div className={classes.cardHolderInfo}>
                <div>
                    <small>Card Holder</small>
                    <div className={'text-uppercase'}>{holderName}</div>
                </div>
                <div>
                    <small>Valid Thru</small>
                    <div>{validTru}</div>
                </div>
            </div>
        </div>
    );
}

export default DebitCard;
