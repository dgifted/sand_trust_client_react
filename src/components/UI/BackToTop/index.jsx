import React from 'react';
import {MdArrowUpward} from "react-icons/md";

function BackToTop() {
    const [visible, toggleVisible] = React.useState(false);
    const scrollDocument = () => {
        window.scrollTo({
            top: 0,
            left: 0,
            behavior: "smooth"
        })
    }
    const setToggleVisibility = () => window.pageYOffset > 100 ? toggleVisible(true) : toggleVisible(false)

    React.useEffect(() => {
        window.addEventListener('scroll', setToggleVisibility);

        return () => window.removeEventListener('scroll', setToggleVisibility)
    }, []);

    return (
        <>
            {visible ? (
                <button style={{
                    position: 'fixed',
                    bottom: '1rem',
                    right: '1rem',
                    zIndex: '3000'
                }}
                        className={'btn btn-primary btn-rounded p-2'}
                        onClick={scrollDocument}
                >
                    <MdArrowUpward size={'1.3rem'}/>
                </button>
            ) : null}
        </>

    );
}

export default BackToTop;
