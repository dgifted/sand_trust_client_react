import React from 'react';
import emptyListPlaceholder from '../../../assets/images/empty-list.png';

function EmptyList({message, style = {}}) {

    return (
        <div className="d-flex flex-column justify-content-center align-items-center py-5 bg-white w-100" style={style}>
            <img
                src={emptyListPlaceholder}
                alt={''}
                style={{width: '150px', height: '150px'}}
            />
            <h3 className="text-muted my-5">{message ? message : 'Empty List'}</h3>
        </div>
    );
}

export default EmptyList;
