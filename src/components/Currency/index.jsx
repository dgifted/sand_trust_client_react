import React from 'react';
import useSWR from "swr";

function Currency({abbr}) {
    const {data: userProfileData} = useSWR('api/user-profile');
    const currency = userProfileData?.stats?.accsDetails?.currency;
    const currencyAbbr = userProfileData?.stats?.accsDetails?.currencyAbbr;

    const showCurrencyAbbr = !!abbr && currencyAbbr;

    if (!currency && !currencyAbbr) return null;



    return <i dangerouslySetInnerHTML={{__html: showCurrencyAbbr ? currencyAbbr : currency}}/>;
}

export default Currency;
