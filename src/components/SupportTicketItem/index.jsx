import React from 'react';

function SupportTicketItem({author, avatar, excerpt, isActive, onclick, status, title, updatedLast}) {

    return (
        <div className={`support-ticket ${isActive ? 'active' : ''}`} onClick={onclick}>
            <div className="st-meta">
                <small
                    className={`badge badge-${status === 'open' ? 'success' : 'danger'} text-capitalize`}>{status}</small>
            </div>
            <div className="st-body">
                <div className="ticket-content">
                    <h6 className="ticket-title">{title}</h6>
                    <div className="ticket-description" dangerouslySetInnerHTML={{__html: excerpt}}/>
                </div>
            </div>
            <div className="st-foot">
                <span className="label">Author:</span>
                <a className="value with-avatar" href="#">
                    <img alt="" src={avatar}/>
                    <span>{author}</span>
                </a>
                <span className="label">Updated:</span><span className="value">{updatedLast}</span>
            </div>
        </div>
    );
}

export default SupportTicketItem;
