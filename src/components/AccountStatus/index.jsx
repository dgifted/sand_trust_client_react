import React from 'react';

function AccountStatus({status = 'inactive'}) {
    let color;

    switch (status) {
        case 'active':
            color = 'success';
            break;
        case 'dormant':
            color = 'warning';
            break;
        case 'on-hold':
            color = 'dark';
            break;
        case 'suspended':
            color = 'secondary';
            break;
        default:
            color = 'info';
    }

    return (
        <div className={`text-uppercase badge badge-${color} badge-sm`}>
            {status}
        </div>
    );
}

export default AccountStatus;
