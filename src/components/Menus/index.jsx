import React from 'react';
import {NavLink} from "react-router-dom";
import {MdDashboard} from "react-icons/md";
import {MdAttachMoney} from "react-icons/md";
import {MdTrendingUp} from "react-icons/md";
import {MdOutlineSettings} from "react-icons/md";
import {MdOutlineHeadsetMic} from "react-icons/md";
import {FiCreditCard} from 'react-icons/fi';

function MainMenus() {
    return (
        <ul className="main-menu">
            <li className="">
                <NavLink to={'/'}>
                    <div className="icon-w">
                        <MdDashboard/>
                    </div>
                    <span>Dashboard</span>
                </NavLink>
            </li>
            <li className="">
                <NavLink to={'/transfers'}>
                    <div className="icon-w">
                        <MdAttachMoney/>
                    </div>
                    <span>Transfers</span>
                </NavLink>
            </li>
            <li className="">
                <NavLink to={'/cards'}>
                    <div className="icon-w">
                        <FiCreditCard/>
                    </div>
                    <span>Cards</span>
                </NavLink>
            </li>
            <li className="">
                <NavLink to={'/transactions'}>
                    <div className="icon-w">
                        <MdTrendingUp/>
                    </div>
                    <span>Transactions</span>
                </NavLink>
            </li>
            <li className="">
                <NavLink to={'/settings'}>
                    <div className="icon-w">
                        <MdOutlineSettings />
                    </div>
                    <span>Settings</span>
                </NavLink>
            </li>
            <li className="">
                <NavLink to={'/supports'}>
                    <div className="icon-w">
                        <MdOutlineHeadsetMic />
                    </div>
                    <span>Supports</span>
                </NavLink>
            </li>
        </ul>
    );
}

export default MainMenus;
