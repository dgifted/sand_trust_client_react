import React from 'react';
import {RichTextEditor} from "@mantine/rte";
import {Link} from "react-router-dom";
import {ErrorMessage, RequiredMarker} from "../FormHelper";
import {injectStyle} from "../../@helper/validations";
import {toast} from "react-toastify";

function TicketReplyBox({onSubmit, hideText = 'Cancel', openText = 'Open', showTitleField}) {
    const [editorOpen, toggleOpenMobileEditor] = React.useState(false);
    const [content, setContent] = React.useState('');
    const [title, setTitle] = React.useState('');
    const [formValid, setFormValidity] = React.useState(false);

    const onSaveReply = () => {
        if (!showTitleField)
            setFormValidity(!!content);
        if (showTitleField)
            setFormValidity(!!content && !!title)

        if (!formValid) {
            return toast.warn('Please fill in the necessary information.');
        }

        onSubmit({content, title});
    };

    return (
        <div className="ticket-reply">
            {editorOpen ? (
                <>
                    {showTitleField ? (
                        <div className="form-group">
                            <label for="title">Enter short caption for your support. <RequiredMarker/></label>
                            <input
                                className="form-control"
                                onChange={e => setTitle(e.target.value)}
                                style={!title ? injectStyle() : {}}
                            />
                            {!title ? <ErrorMessage message="Enter support title"/> : null}
                        </div>
                    ) : null}
                    <RichTextEditor
                        style={{width: '100%'}}
                        value={content}
                        onChange={(val) => setContent(val)}
                    />
                    {!content ? <ErrorMessage message="Enter support message"/> : null}
                    <button className="btn btn-primary mt-4" type="button" onClick={onSaveReply}>
                        Post
                    </button>
                </>
            ) : null}

            <div className="d-flex justify-content-center">
                <div className="load-more-tickets">
                    <Link to={'/'} onClick={e => {
                        e.preventDefault();
                        setTitle('');
                        setContent('');
                        toggleOpenMobileEditor(!editorOpen);
                    }}>
                        <span>{editorOpen ? hideText : openText}</span>
                    </Link>
                </div>
            </div>
        </div>
    );
}

export default TicketReplyBox;
