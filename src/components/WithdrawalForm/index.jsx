import React from 'react';
import useSWR from 'swr';
import {useForm} from "@mantine/form";
import {useModals} from "@mantine/modals";

import Currency from "../Currency";
import {postLocalBankLink} from "../../services/banks";
import {toast} from "react-toastify";
import AddAccountModal from "../Modals/AddAccountModal";

function WithdrawalForm({onSubmit, ...otherProps}) {
    const {balance, banks} = otherProps;
    const {data: pendingWithdrawalData} = useSWR('api/user-withdrawal/pending');
    const addLocalBankModals = useModals();

    const pendingWithdrawal = pendingWithdrawalData?.data?.data || [];

    const form = useForm({
        initialValues: {
            amount: 0,
            bank: ''
        },
        validate: {
            amount: (val) => ((val < 1 || val > balance) ? `Entered amount should be at least 1 & less than ${balance}.` : null),
            bank: (val) => (!val ? 'Select a bank or add new bank.' : null)
        }
    });

    const addBankForm = useForm({
        initialValues: {
            bankName: '',
            bankAccountNumber: ''
        },
        validate: {
            bankName: (val) => (!val ? 'Bank name is required' : null),
            bankAccountNumber: (val) => (!val ? 'Bank account number is required' : null)
        }
    });

    const onAddLocalBank = async (val) => {
        addBankForm.validate();

        const {error} = await postLocalBankLink(val);
        if (error)
            return toast.error(error.message);
        toast.success('Bank account awaiting to be linked. We will notify you once done.', {autoClose: false});
    };

    let modalId;
    const openAddLocalBankModals = () => {
        modalId = addLocalBankModals.openModal({
            title: 'Link local bank',
            children: (
                <AddAccountModal
                    modalRef={addLocalBankModals}
                    modalId={modalId}
                    onSubmit={onAddLocalBank}
                />
            )
        });
    };

    const onSubmitHandler = (val) => {
        form.validate();
        onSubmit(val).then(() => form.reset());
    }

    return (
        <form noValidate onSubmit={form.onSubmit(onSubmitHandler)}>
            <h5 className="element-box-header">Withdraw Money</h5>
            <div className="row">
                <div className="col-sm-5">
                    <div className="form-group">
                        <label className="lighter" htmlFor="amount">Select Amount</label>
                        <div className="input-group mb-2 mr-sm-2 mb-sm-0">
                            <input
                                className={`form-control`}
                                style={{
                                    border: form.errors?.amount ? '1px solid red' : ''
                                }}
                                id="amount"
                                placeholder="Enter Amount..."
                                {...form.getInputProps('amount')}
                            />
                            <div className="input-group-append">
                                <div className="input-group-text"><Currency abbr/></div>
                            </div>
                        </div>
                    </div>
                    {form.errors?.amount ? <small className="text-danger my-0">{form.errors?.amount}</small> : null}
                </div>
                <div className="col-sm-7">
                    <div className="form-group">
                        <label className="lighter" htmlFor="bank">Transfer to</label>
                        <select className="form-control" id="bank"
                                style={{
                                    border: form.errors?.bank ? '1px solid red' : ''
                                }}
                                {...form.getInputProps('bank')}
                        >
                            <option value="">Select bank</option>
                            {banks.map(bank => (
                                <option value={bank.id}>{bank.bank_name} - {bank.bank_account_number}</option>
                            ))}
                        </select>
                    </div>
                    {form.errors?.bank ? <small className="text-danger my-0">{form.errors?.bank}</small> : null}
                </div>
            </div>
            <div className="form-buttons-w text-right compact">
                <button className="btn btn-grey" type="button" onClick={() => openAddLocalBankModals(addBankForm)}>
                    <i className="os-icon os-icon-ui-22"/>
                    <span>Add Account</span>
                </button>
                <button
                    className="btn btn-primary"
                    type="submit"
                    disabled={!!pendingWithdrawal?.length}
                    style={{cursor: !!pendingWithdrawal?.length ? 'not-allowed' : 'pointer'}}
                >
                    <span>
                        {!!pendingWithdrawal?.length ? 'Withdrawal pending' : 'Transfer'}
                    </span>
                    <i className="os-icon os-icon-grid-18"/>
                </button>
            </div>
        </form>
    );
}

export default WithdrawalForm;
