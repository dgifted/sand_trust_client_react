import React from 'react';
import {Link, useNavigate} from "react-router-dom";
import {FiBell, FiBellOff} from "react-icons/fi";

import {useAuthUser} from "../../../@hooks/use-auth-user";
import {useUserAccount} from "../../../@hooks/use-account";
import Currency from "../../Currency";
import {useUserNotifications} from "../../../@hooks/use-notification";
import {useUserPreference} from "../../../@hooks/use-preference";
import EmptyList from "../../UI/EmptyList";
import {useAuthContext} from "../../../contexts/auth-context";
import AccountStatus from "../../AccountStatus";
import {useLogo} from "../../../@hooks/use-logo";

function Navbar() {
    const Logo = useLogo();
    const {data: userData} = useAuthUser();
    const {data: userAccountsData} = useUserAccount();
    const {data: userNotificationsData} = useUserNotifications();
    const {data: userPrefData} = useUserPreference();
    const navigate = useNavigate();
    const {signOut} = useAuthContext();

    const [onDropdown, setOnDropdown] = React.useState(false);

    const user = userData?.data;
    const userAccounts = userAccountsData?.data?.data[0];
    const userNotifications = userNotificationsData?.data?.data;
    const userPref = userPrefData?.data;

    const onDropdownMouseEnter = () => {
        if (!userPref?.notification_state)
            return;
        setOnDropdown(true);
    };
    const onDropdownMouseLeave = () => setOnDropdown(false);

    return (
        <div className="top-bar color-scheme-bright">
            <div className="logo-w menu-size">
                <Link className="logo" to="/">
                    <img src={Logo} alt={process.env.REACT_APP_SITE_NAME} style={{ minWidth: '120px'}}/>

                    {/*<div className="logo-element">*/}
                    {/*    <img src={Logo}/>*/}
                    {/*</div>*/}
                    {/*<div className="logo-label">{process.env.REACT_APP_SITE_NAME}</div>*/}
                </Link>
            </div>
            <div className="fancy-selector-w">
                <div className="fancy-selector-current">
                    <div className="fs-main-info">
                        <div className="fs-name">Account number: {userAccounts?.number}</div>
                        <div className="fs-sub">
                            <span>Balance:</span><strong><Currency/>{userAccounts?.balance.toLocaleString()}</strong>
                        </div>
                    </div>
                </div>

            </div>
            <div className="top-menu-controls">
                <div
                    className={`messages-notifications os-dropdown-trigger ${onDropdown ? 'over' : ''} os-dropdown-position-left`}
                    onMouseEnter={onDropdownMouseEnter}
                    onMouseLeave={onDropdownMouseLeave}
                >
                    {!!userPref?.notification_state ? <FiBell/> : <FiBellOff/>}
                    <div
                        className={`new-messages-count ${!userPref?.notification_state ? 'bg-light text-dark' : ''}`}>
                        {userNotifications?.filter(item => Number(item.is_read) === 0).length}
                    </div>
                    <div className="os-dropdown light message-list">
                        <ul>
                            {!!userNotifications?.length ? (
                                <>
                                    {userNotifications?.map((notification) => (
                                        <li key={notification.id.toString()}>
                                            <a href="#" style={{maxWidth: '300px', overflow: 'hidden'}}>
                                                <div className="message-content">
                                                    <h6 className="message-from">{notification.title || 'No Title'}</h6>
                                                    <p
                                                        className="message-title"
                                                        dangerouslySetInnerHTML={{__html: notification.description}}
                                                        style={{
                                                            wordBreak: "break-all",
                                                            display: 'inline-block',
                                                            maxWidth: '100% !important'
                                                        }}
                                                    />
                                                </div>
                                            </a>
                                        </li>
                                    ))}
                                    <div className="d-flex justify-content-center pt-2">
                                        <button className="btn btn-link">Read all</button>
                                    </div>
                                </>
                            ) : (
                                <EmptyList message="No notifications." style={{ minWidth: '250px'}}/>
                            )}

                        </ul>
                    </div>
                </div>
                <div className="logged-user-w">
                    <div className="logged-user-i">
                        <div className="avatar-w">
                            <img alt="" src={user?.avatar} style={{width: '40px', height: '40px'}}/>
                        </div>
                        <div className="logged-user-menu color-style-bright">
                            <div className="logged-user-avatar-info">
                                <div className="avatar-w">
                                    <img alt="" src={user?.avatar} style={{width: '40px', height: '40px'}}/>
                                </div>
                                <div className="logged-user-info-w">
                                    <div className="logged-user-name">{user?.first_name} {user?.last_name}</div>
                                    <AccountStatus status={userAccounts?.status_name}/>
                                </div>
                            </div>
                            <div className="bg-icon"><i className="os-icon os-icon-wallet-loaded"/></div>
                            <ul>
                                <li>
                                    <a href="#" onClick={e => {
                                        e.preventDefault();
                                        navigate('/settings')
                                    }}
                                    >
                                        <i className="os-icon os-icon-user-male-circle2"/>
                                        <span>Profile Details</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" onClick={async e => {
                                        e.preventDefault();
                                        await signOut();
                                    }}>
                                        <i className="os-icon os-icon-signs-11"/>
                                        <span>Logout</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Navbar;
