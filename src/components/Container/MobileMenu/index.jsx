import React from 'react';
import {Link} from 'react-router-dom';
import {useWindowSize} from "../../../@hooks/use-window-size";
import {useLogo} from "../../../@hooks/use-logo";
import {useAuthUser} from "../../../@hooks/use-auth-user";
import {useUserAccount} from "../../../@hooks/use-account";
import AccountStatus from "../../AccountStatus";
import MainMenus from "../../Menus";

function MobileMenu() {
    const {width} = useWindowSize();
    const Logo = useLogo();
    const {data: userData} = useAuthUser();
    const {data: userAccountsData} = useUserAccount();
    const [mobileMenuOpened, toggleMobileMenuOpened] = React.useState(false);

    const onSmallScreen = width < 768;
    const user = userData?.data;
    const userAccounts = userAccountsData?.data?.data[0];


    return (
        <div className="menu-mobile menu-activated-on-click color-scheme-dark">
            <div className="mm-logo-buttons-w">
                <Link className="mm-logo" to="/">
                    <img src={Logo} alt=""/>
                    {/*<span>{process.env.REACT_APP_SITE_NAME}</span>*/}
                </Link>

                <div className="mm-buttons">
                    <div className="content-panel-open">
                        <div className="os-icon os-icon-grid-circles"/>
                    </div>
                    <div className="mobile-menu-trigger" onClick={() => toggleMobileMenuOpened(!mobileMenuOpened)}>
                        <div className="os-icon os-icon-hamburger-menu-1"/>
                    </div>
                </div>
            </div>
            <div className="menu-and-user" style={{ display: onSmallScreen && mobileMenuOpened ? 'block' : 'none'}}>
                <div className="logged-user-w">
                    <div className="avatar-w">
                        <img alt="" src={user?.avatar} style={{width: '40px', height: '40px'}}/>
                    </div>
                    <div className="logged-user-info-w">
                        <div className="logged-user-name">{user?.first_name} {user?.last_name}</div>
                        <AccountStatus status={userAccounts?.status_name}/>
                    </div>
                </div>

                <MainMenus/>
            </div>
        </div>
    );
}

export default MobileMenu;
