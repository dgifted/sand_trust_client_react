import React, {useEffect} from 'react';
import NProgress from 'nprogress';

function LoadingSuspense() {
    useEffect(() => {
        NProgress.start();

        return () => NProgress.done();
    }, []);

    return (
        <div style={{
            width: '100vw',
            height: '100vh',
            backgroundColor: 'transparent',
            position: 'absolute',
            top: '0',
            left: '0',
            zIndex: '1000'
        }}/>
    );
}

export default LoadingSuspense;
