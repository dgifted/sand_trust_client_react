import React from 'react';
import {Helmet} from "react-helmet";
import Navbar from "../Navbar";
import MobileMenu from "../MobileMenu";
import SideMenu from "../SideMenu";
import BackToTop from "../../UI/BackToTop";
import AuthGuard from "../AuthGuard";

function Layout({children, ...restProps}) {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
    return (
        <AuthGuard>
            <Helmet>
                <title>{restProps.title}</title>
                <meta charSet="utf-8"/>
                <meta content="ie=edge" httpEquiv="x-ua-compatible"/>
                <meta content="template language" name="keywords"/>
                <meta content={restProps.siteName} name="author"/>
                <meta content={restProps.description} name="description"/>
            </Helmet>
            <div className="all-wrapper solid-bg-all" style={{minHeight: '100vh'}}>
                <Navbar/>
                <div className="layout-w">
                    <MobileMenu/>
                    <SideMenu/>
                    <div className="content-w">
                        <div className="content-i">
                            <div className="content-box">{children}</div>
                        </div>
                    </div>
                </div>
            </div>
            <BackToTop/>
        </AuthGuard>
    );
}

export default Layout;
