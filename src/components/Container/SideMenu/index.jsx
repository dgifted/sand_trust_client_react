import React from 'react';
import {IconContext} from 'react-icons';
import MainMenus from "../../Menus";
import {useWindowSize} from "../../../@hooks/use-window-size";

function SideMenu() {
    const {width} = useWindowSize();
    const iconSize = (width < 768) ? '0.7rem' : '1.2rem';

    return (
        <IconContext.Provider value={{size: iconSize, className: 'os-icon', color: '#0073ff'}}>
            <div
                className="menu-w color-scheme-light color-style-transparent menu-position-side menu-side-left menu-layout-compact sub-menu-style-over sub-menu-color-bright selected-menu-color-light menu-activated-on-hover menu-has-selected-link">
                <MainMenus/>
            </div>
        </IconContext.Provider>
    );
}

export default SideMenu;
