import React from 'react';
import DataTable from 'react-data-table-component';
import {FiArrowDown} from 'react-icons/fi';

const sortIcon = <FiArrowDown/>

function DataTableBase(props) {
    return (
        <DataTable
            pagination
            sortIcon={sortIcon}
            {...props}
        />
    );
}

export default DataTableBase;
