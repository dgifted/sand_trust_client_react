import React from 'react';
import {Navigate} from 'react-router-dom';
import {useAuthContext} from "../../../contexts/auth-context";

function AuthGuard({children}) {
    const {loggedIn} = useAuthContext();

    if (!loggedIn)
        return <Navigate to="/login"/>

    return (
        <React.Fragment>{children}</React.Fragment>
    );
}

export default AuthGuard;
