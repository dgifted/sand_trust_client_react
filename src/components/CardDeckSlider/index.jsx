import React from 'react';

import Slider from 'react-slick';

// import classes from './CardDeckSlider.module.css';
// import {MdOutlineArrowBack} from "react-icons/md";
// import {MdOutlineArrowForward} from "react-icons/md";
import DebitCard from "../UI/DebitCard";
// import {slickOption} from "../../data/slick-config";

// const NavButton = ({children, onClick}) => (
//     <button style={{background: 'transparent', outline: 'none', border: 'none'}} onClick={onClick}>
//         {children}
//     </button>
// );

const slickConfigOptions = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
};

function CardDeckSlider() {
    return (


        <Slider {...slickConfigOptions}>
            <DebitCard/>
            <DebitCard/>
            <DebitCard/>
        </Slider>
    );
}

export default CardDeckSlider;
