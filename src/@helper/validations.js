import validator from "validator";
import {blacklistedEmailDomains} from "../data/blacklisted-email-domains";

export const validators = {
    email: (emailString) => {
        if (!validator.isEmail(emailString.trim())) return false;
        return blacklistedEmailDomains.every(item => !emailString.includes(item));
    },
    passwords: (password, confirm, minLength = 8) => {
        return !(
            password.length < minLength
            || password !== confirm
            || password.includes('passwor')
            || password.includes('12345')
        )
    },
    string: (stringText) => validator.isAlpha(stringText.replace(' ', ''))
}

export const injectStyle = (addStyle = {}) => ({
    border: '1px solid red',
    ...addStyle
});
