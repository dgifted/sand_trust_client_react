import React, {Suspense} from "react";

import LoadingSuspense from "../components/Container/LoadingSuspense";

export const getRoutePageComponent = (pageComponent) => (<Suspense fallback={<LoadingSuspense/>}>
    {pageComponent}
</Suspense>)
