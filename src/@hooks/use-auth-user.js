import useSWR from 'swr';

export const useAuthUser = () => useSWR('api/auth/user');


