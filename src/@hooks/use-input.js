import {useState} from "react";

export const useInput = (validatorFn) => {
    const [value, setValue] = useState('');
    const [touched, setTouched] = useState(false);

    const onInputChange = (evt) => setValue(evt.target.value);
    const onInputBlur = () => setTouched(true);

    const hasError = touched && !value;
    const isValid = value && validatorFn(value)

    const reset = () => {
        setValue('');
        setTouched(false);
    }

    return {
        value,
        hasError,
        isValid,
        onChange: onInputChange,
        onBlur: onInputBlur,
        reset
    }
}
