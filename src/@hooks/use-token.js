import { useLocalStorage } from '@mantine/hooks';

export function useToken() {
    const [tokenString, setTokenString] = useLocalStorage({
        key: 'token',
        defaultValue: undefined
    });

    return {
        token: tokenString,
        setToken: setTokenString
    }
}
