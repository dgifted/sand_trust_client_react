import useSWR from 'swr';

export const useUserSupports = () =>useSWR('api/support');
