import useSWR from 'swr';

export const useUserNotifications = () => useSWR('api/notification/user-notifications');
