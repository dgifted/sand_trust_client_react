import useSWR from 'swr';

export const useCards = () => useSWR('api/card/user-cards');
