import Logo from '../assets/images/logo.png';
import useSWR from 'swr';

export const useLogo = (variant = 'white') => {
    const {data} = useSWR(`api/site-logo?variant=${variant}`);

    if (!data)
        return Logo;

    return data;
}
