import useSWR from 'swr';

export const useUserPreference = () => useSWR('api/user-preference');
