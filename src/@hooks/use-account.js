import useSWR from 'swr';

export const useUserAccount = () => useSWR('api/account/user-accounts');
