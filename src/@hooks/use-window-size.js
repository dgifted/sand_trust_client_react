import React from "react";


export const useWindowSize = () => {
    const [windowDimension, changeWindowDimension] = React.useState({
        width: window.innerWidth,
        height: window.innerHeight
    });

    const updateDimension = () => {
        changeWindowDimension({
            width: window.innerWidth,
            height: window.innerHeight
        });
    };

    React.useEffect(() => {
        window.addEventListener('resize', updateDimension);

        return () => window.removeEventListener('resize', updateDimension);
    }, []);

    return windowDimension;
}
