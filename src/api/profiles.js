import {callAPI} from "./index";

export const updateUserProfile = async (payload) => await callAPI('api/update-user-profile', 'patch', payload);
export const uploadProfilePicture = async (payload, onUploadProgress = null) => await callAPI('api/upload-photo', 'post', payload, onUploadProgress);

export const requestCreditIncrease = async () => await callAPI('api/credit-limit/request-increase', 'post');
