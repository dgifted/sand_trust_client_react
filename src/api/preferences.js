import {callAPI} from "./index";

export const updateUserLangPreference = async (payload) => await callAPI(
    `api/user-preference/update-lang`,
    'post',
    payload
);

export const updateUserNotificationPreference = async () => await callAPI(
    'api/user-preference/update-notification',
    'post'
);

export const updateUserThemePreference = async () => await callAPI(
    'api/user-preference/update-theme',
    'post'
);
