import {callAPI} from "./index";

export const postLocalBank = async (payload) => await callAPI('api/user-bank', 'post', payload);

export const sendWithdrawalRequest = async (payload) => await callAPI('api/user-withdrawal', 'post', payload);
