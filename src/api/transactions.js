import {callAPI} from "./index";

export const getAllTransaction = () => callAPI('api/transaction');
export const getSingleTransaction = (transId, type) => callAPI(`api/transaction/user-single-transaction/${transId}?type=${type}`);
