import {callAPI} from "./index";

export const requestCard = async (payload) => await callAPI('api/card/request-new', 'post', payload);
export const cancelCardRequest = async (cardRefId) => await callAPI(`api/card/${cardRefId}/cancel-request`, 'delete');
export const activateCard = async (cardId) => await callAPI(`api/card/${cardId}/activate`);
export const deactivateCard = async (cardId) => await callAPI(`api/card/${cardId}/deactivate`);
