import axios from "axios";

let config = {
    authToken: ''
};
const getApiConfig = () => config;

export const updateApiConfig = (newConfig) => {
    config = {
        ...config,
        ...newConfig
    }
}


export const baseUrl = process.env.REACT_APP_API_URL;

export const callAPI = (endPoint, method = 'get', data = null, onUploadProgress = null) => {
    return new Promise((resolve, reject) => {
        axios({
            method,
            url: `${baseUrl}/${endPoint}`,
            headers: {
                Authorization: config.authToken ? `Bearer ${ getApiConfig().authToken }` : ''
            },
            data,
            onUploadProgress
        })
            .then(res => resolve(res.data))
            .catch(error => {
                reject({
                    status: (error?.response && error?.response.status)||'',
                    message: (error?.response && error?.response?.data && error?.response?.data?.error )|| error?.message
                })
            })
    })
};
