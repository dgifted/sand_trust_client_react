import {callAPI} from "./index";

export const getTicketReplies = async (ticketId) => await callAPI(`api/support/${ticketId}/replies`);

export const createSupportTicket = async (payload) => await callAPI('api/support', 'post', payload);

export const postTicketReply = async (payload, ticketId) => await callAPI(
    `api/support/${ticketId}/replies`,
    'post', payload
);
