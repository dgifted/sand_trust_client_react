import {callAPI} from "./index";

export const postTransfer = async (payload) => await callAPI('api/transfer/create', 'post', payload);
export const confirmTransferOtp = async (payload) => await callAPI('api/transfer/confirm', 'post', payload);
export const resendTransferOtp = async (transferId) => await callAPI(`api/transfer/${transferId}/resend-otp`, 'post');
