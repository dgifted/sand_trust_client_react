import React from "react";
import {useNavigate} from 'react-router-dom';

import {useToken} from "../@hooks/use-token";
import {updateApiConfig} from "../api";
import {authService} from "../services/auth";

export const AuthContext = React.createContext({
    loggedIn: false,
    signIn: undefined,
    signOut: undefined,
    signUp: undefined
});


export const AuthProvider = ({children}) => {
    const {token, setToken} = useToken();
    const loggedIn = !!token;
    const navigate = useNavigate();

    React.useEffect(() => {
        if (token)
            updateApiConfig({authToken: token});
    }, [token]);



    const signIn = async (credential) => {
        return await authService.signIn(credential)
    };

    const signOut = async () => {
        await authService.signOut()
            .finally(() => {
                setToken('');
                navigate('/');
            });
    };

    const signUp = async (payload) => {
        return await authService.signUp(payload);
    }

    const value = {loggedIn, signIn, signOut, signUp};

    return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>
};

export const useAuthContext = () => React.useContext(AuthContext);
