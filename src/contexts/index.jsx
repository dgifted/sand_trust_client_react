import React from "react";
import {MantineProvider} from '@mantine/core';
import {ModalsProvider} from "@mantine/modals";

import {AuthProvider} from "./auth-context";

export const AppContexts = ({children}) => {
    return (
        <MantineProvider>
            <ModalsProvider>
                <AuthProvider>
                    {children}
                </AuthProvider>
            </ModalsProvider>
        </MantineProvider>
    );
}
